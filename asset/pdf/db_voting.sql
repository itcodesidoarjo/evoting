-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2019 at 06:18 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_voting`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id_admin` bigint(16) NOT NULL,
  `nik_admin` varchar(255) DEFAULT NULL,
  `nama_admin` varchar(30) DEFAULT NULL,
  `telepon_admin` varchar(13) DEFAULT NULL,
  `username` varchar(10) DEFAULT NULL,
  `email_admin` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `alamat_admin` text,
  `foto_admin` varchar(255) DEFAULT NULL,
  `jk_admin` varchar(2) DEFAULT NULL,
  `tempatlahir_admin` varchar(255) DEFAULT NULL,
  `tgllahir_admin` date DEFAULT NULL,
  `tglreg_admin` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id_admin`, `nik_admin`, `nama_admin`, `telepon_admin`, `username`, `email_admin`, `password`, `alamat_admin`, `foto_admin`, `jk_admin`, `tempatlahir_admin`, `tgllahir_admin`, `tglreg_admin`) VALUES
(8, '1410800162', NULL, NULL, 'admin', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', NULL, NULL, NULL, NULL, NULL, '2018-12-13 11:10:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_anggota`
--

CREATE TABLE IF NOT EXISTS `tbl_anggota` (
  `id_anggota` bigint(16) NOT NULL,
  `nik_anggota` varchar(50) DEFAULT NULL,
  `nama_anggota` varchar(30) DEFAULT NULL,
  `bidang_anggota` varchar(30) DEFAULT NULL,
  `world_anggota` varchar(50) DEFAULT NULL,
  `brand_anggota` varchar(20) DEFAULT NULL,
  `foto_anggota` varchar(255) DEFAULT NULL,
  `telepon_anggota` varchar(13) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `alamat_anggota` text,
  `tglreg_anggota` datetime DEFAULT NULL,
  `email_anggota` varchar(50) DEFAULT NULL,
  `kodever_anggota` varchar(20) DEFAULT NULL,
  `statusver_anggota` varchar(2) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ballot`
--

CREATE TABLE IF NOT EXISTS `tbl_ballot` (
  `id_ballot` bigint(16) NOT NULL,
  `id_kandidat` bigint(16) DEFAULT NULL,
  `id_undangan` bigint(16) DEFAULT NULL,
  `id_admin` bigint(16) DEFAULT NULL,
  `no_ballot` int(2) DEFAULT NULL,
  `level_ballot` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kandidat`
--

CREATE TABLE IF NOT EXISTS `tbl_kandidat` (
  `id_kandidat` bigint(16) NOT NULL,
  `id_anggota` bigint(16) DEFAULT NULL,
  `periode_kandidat` varchar(10) DEFAULT NULL,
  `bidang_kandidat` varchar(30) DEFAULT NULL,
  `visi_kandidat` text,
  `misi_kandidat` text,
  `world_kandidat` varchar(20) DEFAULT NULL,
  `brand_kandidat` varchar(20) DEFAULT NULL,
  `level_kandidat` varchar(150) DEFAULT NULL,
  `tglreg_kandidat` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_perhitungan`
--

CREATE TABLE IF NOT EXISTS `tbl_perhitungan` (
  `id_perhitungan` bigint(16) NOT NULL,
  `id_anggota` bigint(16) DEFAULT NULL,
  `id_ballot` bigint(16) DEFAULT NULL,
  `tglreg_perhitungan` datetime DEFAULT NULL,
  `id_undangan` bigint(16) DEFAULT NULL,
  `suara_spgpram` varchar(11) DEFAULT '0',
  `suara_csokasir` varchar(11) DEFAULT '0',
  `suara_spvcoor` varchar(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_undangan`
--

CREATE TABLE IF NOT EXISTS `tbl_undangan` (
  `id_undangan` bigint(16) NOT NULL,
  `tgl_undangan` date DEFAULT NULL,
  `keteranagan_undangan` text,
  `tentang_undangan` text,
  `level_undangan` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tbl_anggota`
--
ALTER TABLE `tbl_anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `tbl_ballot`
--
ALTER TABLE `tbl_ballot`
  ADD PRIMARY KEY (`id_ballot`),
  ADD KEY `id_kandidat` (`id_kandidat`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indexes for table `tbl_kandidat`
--
ALTER TABLE `tbl_kandidat`
  ADD PRIMARY KEY (`id_kandidat`);

--
-- Indexes for table `tbl_perhitungan`
--
ALTER TABLE `tbl_perhitungan`
  ADD PRIMARY KEY (`id_perhitungan`),
  ADD KEY `id_anggota` (`id_anggota`),
  ADD KEY `id_ballot` (`id_ballot`);

--
-- Indexes for table `tbl_undangan`
--
ALTER TABLE `tbl_undangan`
  ADD PRIMARY KEY (`id_undangan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id_admin` bigint(16) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_anggota`
--
ALTER TABLE `tbl_anggota`
  MODIFY `id_anggota` bigint(16) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tbl_ballot`
--
ALTER TABLE `tbl_ballot`
  MODIFY `id_ballot` bigint(16) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tbl_kandidat`
--
ALTER TABLE `tbl_kandidat`
  MODIFY `id_kandidat` bigint(16) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tbl_perhitungan`
--
ALTER TABLE `tbl_perhitungan`
  MODIFY `id_perhitungan` bigint(16) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `tbl_undangan`
--
ALTER TABLE `tbl_undangan`
  MODIFY `id_undangan` bigint(16) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_ballot`
--
ALTER TABLE `tbl_ballot`
  ADD CONSTRAINT `tbl_ballot_ibfk_2` FOREIGN KEY (`id_kandidat`) REFERENCES `tbl_kandidat` (`id_kandidat`),
  ADD CONSTRAINT `tbl_ballot_ibfk_3` FOREIGN KEY (`id_admin`) REFERENCES `tbl_admin` (`id_admin`);

--
-- Constraints for table `tbl_perhitungan`
--
ALTER TABLE `tbl_perhitungan`
  ADD CONSTRAINT `tbl_perhitungan_ibfk_1` FOREIGN KEY (`id_ballot`) REFERENCES `tbl_ballot` (`id_ballot`),
  ADD CONSTRAINT `tbl_perhitungan_ibfk_2` FOREIGN KEY (`id_anggota`) REFERENCES `tbl_anggota` (`id_anggota`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
