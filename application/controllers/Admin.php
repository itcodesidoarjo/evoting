<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function data_vote()
	{
		// $this->load->view('header.php');
		$this->load->view('admin/data_report2.php');
		// $this->load->view('footer.php');
	}

	public function dt_spv()
	{
		$this->load->view('header.php');
		$this->load->view('admin/spv.php');
		$this->load->view('footer.php');
	}

	public function dt_kasir()
	{
		$this->load->view('header.php');
		$this->load->view('admin/kasir.php');
		$this->load->view('footer.php');
	}

	public function dt_spg()
	{
		$this->load->view('header.php');
		$this->load->view('admin/spg.php');
		$this->load->view('footer.php');
	}



}