<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Undangansms extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{ 
		$this->load->view('header.php');
		$this->load->view('undangan/undangansms.php');
		$this->load->view('footer.php');
	}

	public function sendmsg()
  {
    // $this->form_validation->set_rules('mobile', 'Mobile', 'required');
    $this->form_validation->set_rules('message', 'Message', 'required');
    $this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
    if ($this->form_validation->run()) 
    {
      $mobile = $this->input->post('mobile');
      $message = $this->input->post('message');
      $cekSemua = $this->input->post('check_list');
      $data = $this->input->post();
      
      $tlpn = $this->manggota->cek_anggota($mobile);

      unset($data['submit']);

      if (!empty($cekSemua)) {
        //ambil data semua nomor di database
        $data['telepon_anggota'] = $this->manggota->nomer_anggota();
        foreach ($data['telepon_anggota'] as $key => $value) { //kirim satu satu berdasarkan nomor
          
          $id_anggota = $value['id_anggota'];
          $kode       = $this->mvote->kodevote();
          if (empty($kode)) {
            $ver = "kosong";
          }else{
            $ver = $kode[0]->kode;
          }

          /*update anggota untuk insert kode*/
          $data = [
           'kodever_anggota'   => $id_anggota.''.$ver,
           'statusver_anggota' => 0,
          ];
          
          $where = array(
              'id_anggota' => $id_anggota
          );
          $this->manggota->upedit_anggota($data,$where); 
             
          $mess       = $message.' KODE VERIFIKASI: "'.$id_anggota.''.$ver.'"';
          
          $msgencode  = urlencode($mess);
          $mobile     = $value['telepon_anggota'];
          $userkey    = "n3gru9";
          $passkey    = "287njpeabx";
          $router     = "";

          $postdata = array('authkey'=>$userkey,
                    'mobile'=>$value['telepon_anggota'],
                    'message'=>$msgencode,
                    'router'=>$router
                    );
          $url = "https://reguler.zenziva.net/apps/smsapi.php?userkey=$userkey&passkey=$passkey&nohp=$mobile&pesan=$msgencode";;

          $ch  = curl_init();
              curl_setopt_array($ch,array(
                          CURLOPT_URL => $url,
                          CURLOPT_RETURNTRANSFER => TRUE,
                          CURLOPT_POST => TRUE,
                          CURLOPT_POSTFIELDS => $postdata
                ));

          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

          $output = curl_exec($ch);
          if (curl_errno($ch)) {
            echo "error". curl_error($ch);
          }
          curl_close($ch);
        }
      }else{

        if (empty($tlpn)) {
            $this->session->set_flashdata('error', 'SMS gagal terkirim, No Tlpn tidak ada dalam kontak anggota.');
            redirect('undangansms'); 
        }else{
            $id_anggota = $tlpn[0]['id_anggota'];
            $kode       = $this->mvote->kodevote();

            if (empty($kode)) {
              $ver = "kosong";
            }else{
              $ver = $kode[0]->kode;
            }
            /*update anggota untuk insert kode*/
            $data = [
             'kodever_anggota'   => $id_anggota.''.$ver,
             'statusver_anggota' => 0,
            ];
            
            $where = array(
                'id_anggota' => $id_anggota
            );
            $this->manggota->upedit_anggota($data,$where); 

            $mess       = $message.' KODE VERIFIKASI: "'.$id_anggota.''.$ver.'"';

            $msgencode = urlencode($mess);
            $userkey = "n3gru9";
            $passkey = "287njpeabx";
            $router = "";

            $postdata = array('authkey'=>$userkey,
                      'mobile'=>$mobile,
                      'message'=>$msgencode,
                      'router'=>$router
                      );
            $url = "https://reguler.zenziva.net/apps/smsapi.php?userkey=$userkey&passkey=$passkey&nohp=$mobile&pesan=$msgencode";;

            $ch  = curl_init();
                curl_setopt_array($ch,array(
                            CURLOPT_URL => $url,
                            CURLOPT_RETURNTRANSFER => TRUE,
                            CURLOPT_POST => TRUE,
                            CURLOPT_POSTFIELDS => $postdata
                  ));

            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            $output = curl_exec($ch);
            if (curl_errno($ch)) {
              echo "error". curl_error($ch);
            }
          curl_close($ch);
        }
      }

      ?>
        <br>respon ID Mobile : <?php echo $output; ?> pesan sukses di kirim</br>
      <?php
      echo "<script>alert('pesan berhasil di kirim');</script>";

      $this->session->set_flashdata('pesan', $output);
      redirect('undangansms'); 
    } 
    
    else 
    {
      $this->index('undangansms');
    }
  }

}