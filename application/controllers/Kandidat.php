<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kandidat extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		$not['notif'] = $this->mundangan->notifikasi();
		$data['tbl_kandidat'] = $this->mkandidat->data_kandidat();
		$this->load->view('header.php',$not);
		$this->load->view('kandidat/kandidatview.php',$data);
		$this->load->view('footer.php');
	}

	public function tam_kandidat()
	{
		$not['notif'] = $this->mundangan->notifikasi();
		$data['tbl_anggota'] = $this->manggota->data_anggota();
		$this->load->view('header.php',$not);
		$this->load->view('kandidat/tambahkandidat.php',$data);
		$this->load->view('footer.php');
	}

	public function det_kandidat()
	{	
		$not['notif'] = $this->mundangan->notifikasi();
		$this->load->view('header.php',$not);
		$this->load->view('kandidat/detailkandidatview.php');
		$this->load->view('footer.php');
	}


	public function inputkandidat()
	{
      $level_kandidat 	= $this->input->post('level_kandidat');
      $bidang_kandidat 	= $this->input->post('bidang_kandidat');
      $periode_kandidat = $this->input->post('periode_kandidat');
      $id 				= $this->input->post('id_anggota');

		if (empty($id)) {
		  	$this->session->set_flashdata('error','Silahkan pilih kandidat terlebih dahulu.');
			redirect('kandidat/tam_kandidat');
		}else{
	  		$cek      	= $this->mkandidat->cekdata_kandidat($id,$periode_kandidat,$level_kandidat); 

		  	if (empty($cek)) {
		 		$cekbidang1   = $this->mkandidat->cek_bidang1($id,$bidang_kandidat,$level_kandidat); 
		 		$cekbidang2   = $this->mkandidat->cek_bidang2($id,$bidang_kandidat,$level_kandidat); 
		 		$cekbidang3   = $this->mkandidat->cek_bidang3($id,$bidang_kandidat,$level_kandidat); 

				if (!empty($cekbidang1)) {
					if ($cekbidang1[0]->nama != $level_kandidat) {
						$this->session->set_flashdata('error','Gagal disimpan, SPG PRAM tidak bisa menjadi kandidat CSO KASIR dan SPV COOR.');
			      		redirect('kandidat/tam_kandidat');
					}else{
						date_default_timezone_set('Asia/Jakarta');
				     	$jam = date("Y-m-d h:i:s");
					      $data = [
					       'periode_kandidat' 	=> set_value('periode_kandidat'),
					       'bidang_kandidat'	=> set_value('bidang_kandidat'),
					       'visi_kandidat' 		=> set_value('visi_kandidat'),
					       'misi_kandidat' 		=> set_value('misi_kandidat'),
					       'world_kandidat'		=> set_value('world_kandidat'),
					       'brand_kandidat' 	=> set_value('brand_kandidat'),
					       'level_kandidat' 	=> set_value('level_kandidat'),
					       'id_anggota' 		=> set_value('id_anggota'),
					       'tglreg_kandidat'	=> $jam,
					     ];
					     	
					      $this->mkandidat->input_kandidat($data); 
				          $this->session->set_flashdata('pesan','Berhasil disimpan.');
					      redirect('kandidat');
					}
				}else if(!empty($cekbidang2)){
			     	if ($cekbidang2[0]->nama != $level_kandidat) {
						$this->session->set_flashdata('error','Gagal disimpan, CSO KASIR tidak bisa menjadi kandidat SPG PRAM dan SPV COOR.');
			      		redirect('kandidat/tam_kandidat');
					}else{
						date_default_timezone_set('Asia/Jakarta');
				     	$jam = date("Y-m-d h:i:s");
					      $data = [
					       'periode_kandidat' 	=> set_value('periode_kandidat'),
					       'bidang_kandidat'	=> set_value('bidang_kandidat'),
					       'visi_kandidat' 		=> set_value('visi_kandidat'),
					       'misi_kandidat' 		=> set_value('misi_kandidat'),
					       'world_kandidat'		=> set_value('world_kandidat'),
					       'brand_kandidat' 	=> set_value('brand_kandidat'),
					       'level_kandidat' 	=> set_value('level_kandidat'),
					       'id_anggota' 		=> set_value('id_anggota'),
					       'tglreg_kandidat'	=> $jam,
					     ];
					     	
					      $this->mkandidat->input_kandidat($data); 
				          $this->session->set_flashdata('pesan','Berhasil disimpan.');
					      redirect('kandidat');
					}
			    }else if (!empty($cekbidang3)) {
			    	if ($cekbidang3[0]->nama != $level_kandidat) {
						$this->session->set_flashdata('error','Gagal disimpan, SPV COOR tidak bisa menjadi kandidat SPG PRAM dan CSO KASIR.');
			      		redirect('kandidat/tam_kandidat');
					}else{
						date_default_timezone_set('Asia/Jakarta');
				     	$jam = date("Y-m-d h:i:s");
					      $data = [
					       'periode_kandidat' 	=> set_value('periode_kandidat'),
					       'bidang_kandidat'	=> set_value('bidang_kandidat'),
					       'visi_kandidat' 		=> set_value('visi_kandidat'),
					       'misi_kandidat' 		=> set_value('misi_kandidat'),
					       'world_kandidat'		=> set_value('world_kandidat'),
					       'brand_kandidat' 	=> set_value('brand_kandidat'),
					       'level_kandidat' 	=> set_value('level_kandidat'),
					       'id_anggota' 		=> set_value('id_anggota'),
					       'tglreg_kandidat'	=> $jam,
					     ];
					     	
					      $this->mkandidat->input_kandidat($data); 
				          $this->session->set_flashdata('pesan','Berhasil disimpan.');
					      redirect('kandidat');
					}
			    }
		  	}else{
		  		$this->session->set_flashdata('error','Gagal disimpan, Data kandidat sudah terdaftar.');
			    redirect('kandidat/tam_kandidat');
		  	}
		 }
	}

	public function detailkandidat($id)
	{	
		$not['notif'] = $this->mundangan->notifikasi();
		$data['data_kandidat'] = $this->mkandidat->detail_kandidat($id);
		$this->load->view('header.php',$not);
		$this->load->view('kandidat/detailkandidatview.php',$data);
		$this->load->view('footer.php');
	}

	public function hapuskandidat($id)
	{
		$hapus = $this->mkandidat->hapus_kandidat($id);
		if (empty($hapus)) {
			$this->session->set_flashdata('pesan','Data kandidat berhasil di hapus');
			redirect('kandidat');
		}else{
			$this->session->set_flashdata('error','Data kandidat telah digunakan tidak bisa di hapus');
			redirect('kandidat');
		}
	}

	public function editkandidat($id)
	{
		$not['notif'] = $this->mundangan->notifikasi();
		$data['data_kandidat'] = $this->mkandidat->edit_kandidat($id);
		$this->load->view('header.php',$not);
		$this->load->view('kandidat/editkandidat.php',$data);
		$this->load->view('footer.php');
	}

	public function upeditkandidat($id)
	{
	
      	$level_kandidat 	= $this->input->post('level_kandidat');
      	$bidang_kandidat 	= $this->input->post('bidang_kandidat');

      	if ($bidang_kandidat="SPG PRAM") {
			if ($bidang_kandidat != $level_kandidat) {
				$this->session->set_flashdata('error','Gagal disimpan, SPG PRAM tidak bisa menjadi kandidat CSO KASIR dan SPV COOR.');
	      		redirect('kandidat');
			}else{
		     date_default_timezone_set('Asia/Jakarta');
		     $jam = date("Y-m-d h:i:s");
		     $data = [
		       'periode_kandidat' 	=> set_value('periode_kandidat'),
		       'bidang_kandidat'	=> set_value('bidang_kandidat'),
		       'visi_kandidat' 		=> set_value('visi_kandidat'),
		       'misi_kandidat' 		=> set_value('misi_kandidat'),
		       'world_kandidat'		=> set_value('world_kandidat'),
		       'brand_kandidat' 	=> set_value('brand_kandidat'),
		       'level_kandidat' 	=> set_value('level_kandidat'),
		     ];
		     	
		      $where = array(
					'id_kandidat' => $id
				);
		      $this->mkandidat->upedit_kandidat($data,$where); 
	          $this->session->set_flashdata('pesan','Berhasil disimpan.');
		      redirect('kandidat'); 
		    }
		}else{

			 date_default_timezone_set('Asia/Jakarta');
		     $jam = date("Y-m-d h:i:s");
		     $data = [
		       'periode_kandidat' 	=> set_value('periode_kandidat'),
		       'bidang_kandidat'	=> set_value('bidang_kandidat'),
		       'visi_kandidat' 		=> set_value('visi_kandidat'),
		       'misi_kandidat' 		=> set_value('misi_kandidat'),
		       'world_kandidat'		=> set_value('world_kandidat'),
		       'brand_kandidat' 	=> set_value('brand_kandidat'),
		       'level_kandidat' 	=> set_value('level_kandidat'),
		     ];
		     	
		      $where = array(
					'id_kandidat' => $id
				);
		      $this->mkandidat->upedit_kandidat($data,$where); 
	          $this->session->set_flashdata('pesan','Berhasil disimpan.');
		      redirect('kandidat'); 
		}
	}

	public function cetak()
	{
		$not['notif'] = $this->mundangan->notifikasi();
		$this->load->view('header.php',$not);
		$this->load->view('kandidat/cetakkandidat.php');
		$this->load->view('footer.php');
	}

	public function cetakpdf()
	{
		$data['tbl_kandidat'] = $this->mkandidat->data_kandidat();
		$this->load->view('kandidat/cetak.php',$data);
	}

}