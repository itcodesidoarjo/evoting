<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vote extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{	
		$id = $this->session->userdata('ses_id_anggota');
		$not['notif'] = $this->mundangan->notifikasi();
		$data['vote'] = $this->mvote->data_vote();
		$data['anggotastatus'] = $this->mvote->cekvery($id);

		$data['pram'] 	= $this->mvote->data_votepram();
		$data['kasir'] 	= $this->mvote->data_votekasir();
		$data['spv']	= $this->mvote->data_votespv();


		$data['hitungpram'] 	= $this->mvote->hitungpram($id);
		$data['hitungkasir'] 	= $this->mvote->hitungkasir($id);
		$data['hitungspv']		= $this->mvote->hitungspv($id);

		$this->load->view('header.php',$not);
		$this->load->view('vote/jadwalvote.php',$data);
		$this->load->view('footer.php');
	}

	// public function daftarvote($id)
	// {   
	// 	$not['notif'] = $this->mundangan->notifikasi();
	// 	$anggota= $this->session->userdata('ses_id_anggota');
	// 	$data['cek_vote'] = $this->mvote->cekvote($id,$anggota);
	// 	$data['data_vote'] = $this->mvote->votedata($id);
	// 	$this->load->view('header.php',$not);
	// 	$this->load->view('vote/tambahvote.php',$data);
	// 	$this->load->view('footer.php');
	// }

	public function voting()
	{
		date_default_timezone_set('Asia/Jakarta');


		$data1 = [
	       'statusver_anggota' 			=> 0,
	    ];	

    	$where = array(
			'id_anggota' 		=>  $this->session->userdata('ses_id_anggota'),
		);
	    $this->mvote->update_verifikasi($data1,$where); 

	    $cek = $this->mvote->cek_ver($this->session->userdata('ses_id_anggota')); 

	    if (!empty($cek)) {
	    	$data = [
		       'id_anggota' 	    => $this->session->userdata('ses_id_anggota'),
		       'id_undangan' 		=> set_value('id_undangan'),
		       'tglreg_perhitungan'	=> date("Y-m-d h:i:sa"),
		       'id_ballot' 			=> set_value('id_ballot'),
		       'suara_spvcoor' 		=> set_value('suara2'),
		       'suara_csokasir' 	=> set_value('suara1'),
		       'suara_spgpram' 		=> set_value('suara'),

		    ];

        	$this->session->set_userdata('ses_data_vote',$data);

        	if (!empty($cek[0]->telepon_anggota)) 
		    {
		    	 $mobile = $cek[0]->telepon_anggota;

			   	$i 		= date("i");
			   	$s 		= date("s");
				$data2 	= [
				   'kodever_anggota'   	=> $s.''.$this->session->userdata('ses_id_anggota').''.$i,
				   'statusver_anggota' 	=> 0,
				];

				$where = array(
			  		'id_anggota' => $this->session->userdata('ses_id_anggota'),
				);

				$this->manggota->upedit_ver($data2,$where); 

					/*sms*/
				$mess       = $message.' KODE VERIFIKASI : "'.$s.''.$this->session->userdata('ses_id_anggota').''.$i.'"';

				$msgencode = urlencode($mess);
				$userkey = "n3gru9";
				$passkey = "287njpeabx";
				$router = "";

				$postdata = array('authkey'=>$userkey,
				          'mobile'=>$mobile,
				          'message'=>$msgencode,
				          'router'=>$router
				          );
				$url = "https://reguler.zenziva.net/apps/smsapi.php?userkey=$userkey&passkey=$passkey&nohp=$mobile&pesan=$msgencode";;

				$ch  = curl_init();
				    curl_setopt_array($ch,array(
				                CURLOPT_URL => $url,
				                CURLOPT_RETURNTRANSFER => TRUE,
				                CURLOPT_POST => TRUE,
				                CURLOPT_POSTFIELDS => $postdata
				      ));

				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

				$output = curl_exec($ch);
				if (curl_errno($ch)) {
				  echo "error". curl_error($ch);
				}
				curl_close($ch);

				$this->session->set_flashdata('pesan', 'Silakan masukkan kode verifikasi.');
	    		redirect('welcome/verifikas_anggota');

		    }else{
			    $this->session->set_flashdata('error', 'Nomer telpon tidak ada.');
			    redirect('vote'); 
		    }
	    }else{
	    	 $this->session->set_flashdata('error', 'Gagal Vote.');
	    	redirect('vote'); 
	    }

	}

		public function inputverifikasi()
	{
		date_default_timezone_set('Asia/Jakarta');
	     $data = [
	       'statusver_anggota' 			=> 1,
	    ];	

    	$where = array(
			'id_anggota' 		=>  $this->session->userdata('ses_id_anggota'),
			'kodever_anggota' 	=>  $this->input->post('kode'),
		);
	    $this->mvote->update_verifikasi($data,$where); 
	    $this->session->set_flashdata('pesan', 'Verifikasi berhasil.');
	    redirect('vote'); 
	}

	public function inputver()
	{
		date_default_timezone_set('Asia/Jakarta');
	    $cek = $this->mvote->cek_inputver($this->input->post('kode'),$this->session->userdata('ses_id_anggota')); 
	    if (!empty($cek)) {
		    $this->mvote->input_vote($this->session->userdata('ses_data_vote')); 
		    $this->session->set_flashdata('pesan', 'Vote Berhasil Terkirim.');
		    $this->session->unset_userdata('ses_data_vote');
		    redirect('vote');
	    }else{
	    	$this->session->set_flashdata('pesan', 'Kode verifikasi salah.');
		    redirect('vote');
	    }
	     
	}

	public function cetak()
	{
		$not['notif'] = $this->mundangan->notifikasi();
		$this->load->view('header.php',$not);
		$this->load->view('vote/cetakvote.php');
		$this->load->view('footer.php');
	}

	public function cetakpdf()
	{
		$data['tbl_vote'] = $this->mvote->datavotecetak();
		$this->load->view('vote/cetak.php',$data);
	}

	public function logver()
	{
	    $data = $this->input->post('kode');

		if($this->session->userdata('logged_anggota')){
			$cek  = $this->mvote->verlog($data);
		}

		if($this->session->userdata('logged_in')){
			$cek  = $this->mvote->verlogadmin($data);
		}

		if (!empty($cek)) {
            $this->session->set_userdata('ses_ver',1);

			date_default_timezone_set('Asia/Jakarta');
		     $data = [
		       'log_statusadmin' 			=> 1,
		    ];	

	    	$where = array(
				'log_veradmin' 	=>  $this->input->post('kode'),
			);
   //          if($this->session->userdata('logged_anggota')){
		 //    	$this->mvote->update_verifikasi($data,$where); 
			// }

			if($this->session->userdata('logged_in')){
		    	$this->mvote->update_verifikasiadmin($data,$where); 
			}

		    $this->session->set_flashdata('pesan', 'Verifikasi berhasil.');
		    redirect('welcome');
		}else{
			$this->session->set_flashdata('error', 'Verifikasi gagal.');
		    redirect('welcome/verifikasi');
		}
	}

}