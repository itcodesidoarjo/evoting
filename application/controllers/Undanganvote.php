<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Undanganvote extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{    
		$not['notif'] = $this->mundangan->notifikasi();
		$data['vote'] = $this->mundangan->data_vote();
		$this->load->view('header.php',$not);
		$this->load->view('undangan/undanganvote.php',$data);
		$this->load->view('footer.php');
	}

	public function tambahundangan()
	{
		$not['notif'] = $this->mundangan->notifikasi();
		$data['tbl_undangan'] = $this->mundangan->data_undangan();
		$data['tbl_kandidat'] = $this->mkandidat->data_kandidat();
		$this->load->view('header.php',$not);
		$this->load->view('undangan/tambahundangan.php',$data);
		$this->load->view('footer.php');
	}

	public function tambahjadwal()
	{
		$not['notif'] = $this->mundangan->notifikasi();
		$this->load->view('header.php',$not);
		$this->load->view('undangan/tambahjadwal.php');
		$this->load->view('footer.php');
	}

	public function inputjadwal()
	{
		$originalDate = $this->input->post('tgl_undangan');
		$newDate = date("Y-m-d", strtotime($originalDate));	

	    $cek = $this->mundangan->cek_tglundangan($newDate);

	    if (empty($cek)) {
	    	$data = [
		       'tgl_undangan' 			=> $newDate,
		       'tentang_undangan' 		=> set_value('tentang_undangan'),
		       'keteranagan_undangan'	=> set_value('keteranagan_undangan'),
		       'level_undangan'			=> set_value('level_undangan'),
		    ];
	        $this->session->set_flashdata('pesan','Berhasil Disimpan');
		    $this->mundangan->input_undangan($data); 
		    redirect('undanganvote/tambahundangan'); 
	    }else{
	        $this->session->set_flashdata('error','Gagal disimpan, tanggal yang anda pilih sudah ada vote. Silahkan cari tanggal yang lain! ');
	    	redirect('undanganvote/tambahjadwal'); 
	    }
	}

	public function inputballot()
	{	
		$data['id_undangan'] 	= $this->input->post('id_undangan');
		$data['id_kandidat'] 	= $this->input->post('id_kandidat');
		$data['no_ballot']   	= $this->input->post('no_ballot');
		/*perlu*/
		$data['level_ballot'] 	= $this->input->post('level_ballot');
		$data['tgl_undangan'] 	= $this->input->post('tgl_undangan');

		$periode = date('Y', strtotime($data['tgl_undangan']));
		$newDate = date("Y-m-d", strtotime($data['tgl_undangan']));
		/*cek data kandidat*/
		$dat1 	= $this->mundangan->cek_tanggalperiode($periode,$data['level_ballot']);
		if (empty($dat1)) {
			$this->session->set_flashdata('error','Gagal disimpan, tidak ada data kandidat yang sesuai dengan periode dan jenis yang dipilih.');
	    	redirect('undanganvote/tambahundangan'); 
		}else{
			$undangan = $this->mundangan->createcek_undangan($newDate);
			if (!empty($undangan)) {
				$id_undangan = $undangan[0]->id_undangan;

				$undangan = $this->mundangan->cekcreate_ballot($id_undangan,$data['level_ballot']);
				if (empty($undangan)) {
					$no=1;
					foreach ($dat1 as $value) {
						 $data1 = [
					       'id_undangan' 		=> $id_undangan,
					       'id_kandidat' 		=> $value->id_kandidat,
					       'no_ballot'			=> $no++,
					       'id_admin'			=> set_value('id_admin'),
					       'level_ballot'		=> set_value('level_ballot'),
					    ];
					    $this->mundangan->input_ballot($data1); 
					}
					$this->session->set_flashdata('pesan','Berhasil Disimpan');
					redirect('undanganvote');
				}else{
					$this->session->set_flashdata('error','Gagal Tersimpan, tanggal undangan sudah pernah digunakan');
	    			redirect('undanganvote/tambahundangan');
				}
			}else{
				
				$undangantahun = $this->mundangan->cektahun_undangan($periode);

				if (empty($undangantahun)) {
					$data = [
			       		'tgl_undangan' 			=> $newDate,
				    ];
				    $dataundangan = $this->mundangan->create_undangan($data); 

				    $no=1;
					foreach ($dat1 as $value) {
						 $data1 = [
					       'id_undangan' 		=> $dataundangan,
					       'id_kandidat' 		=> $value->id_kandidat,
					       'no_ballot'			=> $no++,
					       'id_admin'			=> set_value('id_admin'),
					       'level_ballot'		=> set_value('level_ballot'),
					    ];
					    $this->mundangan->input_ballot($data1); 
					}
					$this->session->set_flashdata('pesan','Berhasil Disimpan');
					redirect('undanganvote');
				}else{
					$this->session->set_flashdata('error','Gagal disimpan, periode tahun ini sudah ada atau sesuaikan dengan tanggal yang ada di data undangan vote untuk periode tahun yang dipilih.');
	    				redirect('undanganvote/tambahundangan'); 
				}
			}
		}

		// if (empty($data['id_undangan']) || empty($data['id_kandidat']) || empty($data['no_ballot'])) {
  //         	$this->session->set_flashdata('error','Gagal disimpan, semua kolom harus di isi.');
	 //    	redirect('undanganvote/tambahundangan'); 
		// }else{
		// 	$dat1 = $this->mundangan->cek_ballot($data['id_undangan'],$data['id_kandidat']);
		// 	$dat2 = $this->mundangan->cek_ballotdua($data['id_undangan'],$data['no_ballot'],$data['level_ballot']);
			
		// 		if (!empty($dat1)) {
  //         			$this->session->set_flashdata('error','Gagal Tersimpan, Kandidat sudah ada dalam voting');
	 //    			redirect('undanganvote/tambahundangan'); 
		// 		}else if (!empty($dat2)) {
  //         			$this->session->set_flashdata('error','Gagal Tersimpan, No. kandidat sudah di ada dalam voting');
		// 			redirect('undanganvote/tambahundangan'); 
		// 		}else{
		// 			 $data1 = [
		// 		       'id_undangan' 		=> set_value('id_undangan'),
		// 		       'id_kandidat' 		=> set_value('id_kandidat'),
		// 		       'no_ballot'			=> set_value('no_ballot'),
		// 		       'id_admin'			=> set_value('id_admin'),
		// 		       'level_ballot'		=> set_value('level_ballot'),
		// 		    ];
		// 		    $this->mundangan->input_ballot($data1); 
  //         			$this->session->set_flashdata('pesan','Berhasil Disimpan');
		// 		    redirect('undanganvote'); 
		// 		}
		// }
	}

	public function detailundangan($id)
	{	
		$data['data_anggota'] = $this->mundangan->detail_undangan($id);
		$this->load->view('header.php');
		$this->load->view('undangan/detailundangan.php',$data);
		$this->load->view('footer.php');
	}

	public function editundangan($id)
	{
		$not['notif'] = $this->mundangan->notifikasi();
		$data['tbl_undangan'] = $this->mundangan->data_undangan();
		$data['tbl_kandidat'] = $this->mkandidat->data_kandidat();
		$data['data_anggota'] = $this->mundangan->detail_undangan($id);
		$data['vote'] 		  = $this->mundangan->data_editvote($id);
		$this->load->view('header.php');
		$this->load->view('undangan/editundangan.php',$data);
		$this->load->view('footer.php');
	}

	public function updateundangan()
	{
		$id				= $this->input->post('id_ballot');
		$no_ballot		= $this->input->post('no_ballot');
		$level_kandidat	= $this->input->post('level_kandidat');
		$tgl			= $this->input->post('tgl');

      	$cek = $this->mundangan->cek_ball($no_ballot,$level_kandidat,$tgl); 

      	if (empty($cek)) {
      		$data = [
	       		'no_ballot'=> set_value('no_ballot'),
	     	];
	      	$where = array(
				'id_ballot' => $id
			);

	      $this->mundangan->up_ball($data,$where); 
	      $this->session->set_flashdata('pesan','Berhasil disimpan.');
	      redirect('undanganvote');
      	}else{
      		$this->session->set_flashdata('error','Gagal, nomor sudah ada.');
	      	redirect('undanganvote');
      	}
		
    }

}