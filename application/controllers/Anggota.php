<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {

	public function index()
	{	
		$not['notif'] = $this->mundangan->notifikasi();
		$data['tbl_anggota'] = $this->manggota->data_anggota();
		$this->load->view('header.php',$not);
		$this->load->view('admin/adm_dtanggota.php',$data);
		$this->load->view('footer.php');
	}

	public function tambahanggota()
	{
		$not['notif'] = $this->mundangan->notifikasi();
		$this->load->view('header.php',$not);
		$this->load->view('admin/tambah_dtanggota.php');
		$this->load->view('footer.php');
	}

	public function detailanggota($id)
	{	
		$not['notif'] = $this->mundangan->notifikasi();
		$data['data_anggota'] = $this->manggota->detail_anggota($id);
		$this->load->view('header.php',$not);
		$this->load->view('admin/detail_dtanggota.php',$data);
		$this->load->view('footer.php');
	}

	public function inputanggota()
	{

	 	$data['telepon_anggota'] = $this->input->post('telepon_anggota');
	 	$data['nik_anggota'] 	 = $this->input->post('nik_anggota');

	 	$cek 	= $this->manggota->cek_anggota($data['telepon_anggota']);
	 	$cekno 	= $this->manggota->cek_no($data['nik_anggota']);
	 	if (!empty($cekno)) {
	 		$this->session->set_flashdata('error','No. Identitas sudah digunakan.');
	       	redirect('anggota/tambahanggota');
	 	}else{
		 	if (!empty($cek)) {
		        $this->session->set_flashdata('error','No. Telp sudah digunakan.');
		       	redirect('anggota/tambahanggota'); 
		 	}else{
			 	$config = [
			        'upload_path' 	=> './asset/img/',
			        'allowed_types' => 'gif|jpg|png|jpeg|bmp',
			        'max_size' 		=> 5000, 
			        'remove_space'	=> TRUE,
			      	];
			      	$this->load->library('upload', $config);
			      	if (!$this->upload->do_upload()) //jika gagal upload
			      	{
				         $error = array('error' => $this->upload->display_errors()); //tampilkan error

				        $this->session->set_flashdata('error','Foto anda tidak sesui dengan format!');
				       	redirect('anggota/tambahanggota'); 

			      	}else{		
				      	$file = $this->upload->data();
				     	date_default_timezone_set('Asia/Jakarta');
				     	$jam = date("Y-m-d h:i:s");
				     	$enc_password = md5($this->input->post('password'));
				     	
					      $data = [
					       'foto_anggota' 		=> $file['file_name'],
					       'nik_anggota' 	=> set_value('nik_anggota'),
					       'nama_anggota' 	=> set_value('nama_anggota'),
					       'bidang_anggota'	=> set_value('bidang_anggota'),
					       'world_anggota' 	=> set_value('world_anggota'),
					       'brand_anggota' 	=> set_value('brand_anggota'),
					       'telepon_anggota'=> set_value('telepon_anggota'),
					       'username' 		=> set_value('username'),
					       'password' 		=> $enc_password,
					       'email_anggota' 	=> set_value('email_anggota'),
					       'alamat_anggota' => set_value('alamat_anggota'),
					       'tglreg_anggota' => $jam,
					     ];
				     	
					      $this->manggota->input_anggota($data); 
				          $this->session->set_flashdata('pesan','Berhasil disimpan.');
					      redirect('anggota'); 
					}
		 	}
		}
	}

	public function hapusanggota($id)
	{
		$hapus = $this->manggota->hapus_anggota($id);
		if (empty($hapus)) {
			$this->session->set_flashdata('pesan','Data anggota berhasil di hapus');
			redirect('anggota');
		}else{
			$this->session->set_flashdata('error','Data anggota telah digunakan tidak bisa di hapus');
			redirect('anggota');
		}
		
	}

	public function editanggota($id)
	{
		$not['notif'] = $this->mundangan->notifikasi();
		$data['data_anggota'] = $this->manggota->edit_anggota($id);
		$this->load->view('header.php',$not);
		$this->load->view('anggota/edit_anggota.php',$data);
		$this->load->view('footer.php');
	}

	public function upsimpananggota($id){

		$data['telepon_anggota'] = $this->input->post('telepon_anggota');
 		$data['nik_anggota'] 	 = $this->input->post('nik_anggota');

 		$cek 	= $this->manggota->cek_anggotaup($data['telepon_anggota'],$id);
 		$cekno 	= $this->manggota->cek_noup($data['nik_anggota'],$id);

 		if (!empty($cekno)) {
	 		$this->session->set_flashdata('error','No. Identitas sudah digunakan.');
	       	redirect('anggota/editanggota/'.$id);
	 	}else{
		 	if (!empty($cek)) {
		        $this->session->set_flashdata('error','No. Telp sudah digunakan.');
		       	redirect('anggota/editanggota'.$id); 
		 	}else{
			  	$config = [
				    'upload_path' 	=> './asset/img/',
				    'allowed_types' => 'gif|jpg|png|jpeg|bmp',
				    'max_size' 		=> 5000, 
				    'remove_space'	=> TRUE,
			  	];
		    	$this->load->library('upload', $config);
		    	
		    	if (!$this->upload->do_upload())
		    	{
		         //  	$error = array('error' => $this->upload->display_errors());

		       	 // 	$this->session->set_flashdata('error','Foto anda tidak sesui dengan format!');
		       		// redirect('anggota/editanggota/'.$id);
		       			date_default_timezone_set('Asia/Jakarta');
			          	$data = [
					       'nik_anggota' 	=> set_value('nik_anggota'),
					       'nama_anggota' 	=> set_value('nama_anggota'),
					       'bidang_anggota'	=> set_value('bidang_anggota'),
					       'world_anggota' 	=> set_value('world_anggota'),
					       'brand_anggota' 	=> set_value('brand_anggota'),
					       'telepon_anggota'=> set_value('telepon_anggota'),
					       'email_anggota' 	=> set_value('email_anggota'),
					       'alamat_anggota' 	=> set_value('alamat_anggota'),
			         	];
				      	$where = array(
							'id_anggota' => $id
						);
						
				      $this->manggota->upedit_anggota($data,$where); 
			          $this->session->set_flashdata('pesan','Berhasil disimpan.');
				      redirect('anggota');

		      	}else{		
		          	$file = $this->upload->data();
				  	date_default_timezone_set('Asia/Jakarta');
		          	$data = [
			           'foto_anggota' 		=> $file['file_name'],
				       'nik_anggota' 	=> set_value('nik_anggota'),
				       'nama_anggota' 	=> set_value('nama_anggota'),
				       'bidang_anggota'	=> set_value('bidang_anggota'),
				       'world_anggota' 	=> set_value('world_anggota'),
				       'brand_anggota' 	=> set_value('brand_anggota'),
				       'telepon_anggota'=> set_value('telepon_anggota'),
				       'email_anggota' 	=> set_value('email_anggota'),
				       'alamat_anggota' 	=> set_value('alamat_anggota'),
		         	];
			      	$where = array(
						'id_anggota' => $id
					);
			      $this->manggota->upedit_anggota($data,$where); 
		          $this->session->set_flashdata('pesan','Berhasil disimpan.');
			      redirect('anggota');
		      	}
		    }
		}
  	}

	public function cetak()
	{
		$not['notif'] = $this->mundangan->notifikasi();
		$this->load->view('header.php',$not);
		$this->load->view('anggota/cetakanggota.php');
		$this->load->view('footer.php');
	}

	public function cetakpdf()
	{
		$data['tbl_anggota'] = $this->manggota->data_anggota();
		$this->load->view('anggota/cetak.php',$data);
	}
}