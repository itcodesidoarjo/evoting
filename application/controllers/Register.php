<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        if($this->session->userdata('status') == "login"){
            redirect(base_url("welcome"));
        }
    }
    
	public function index()
	{
		$data['title']='Registrasi';

		$this->form_validation->set_rules('nik_admin', 'NIK','required|numeric|callback_check_userid_exists');
		$this->form_validation->set_rules('username', 'Username', 'required|callback_check_username_exists');
		$this->form_validation->set_rules('email_admin', 'Email', 'required|callback_check_email_exists');
		$this->form_validation->set_rules('password1', 'Password', 'required|min_length[4]');
		$this->form_validation->set_rules('password2', 'Confirm Password', 'matches[password1]');
	
		if($this->form_validation->run() === FALSE){
				$this->load->view('register.php');
			} else {

				// Encrypt password
				$enc_password = md5($this->input->post('password1'));

				$this->mregister->register($enc_password);

				// Set set_message
				$this->session->set_flashdata('user_masuk', 'You are now registered and can log in');
				redirect(base_url("login"));
			}
	}

	// Check if username exists
		public function check_username_exists($username){
			$this->form_validation->set_message('check_username_exists', 'Userneme yang anda masukkan telah digunakan!');
			if($this->mregister->check_username_exists($username)){
				return true;
			} else {
				return false;
			}
		}

		// Check if email exists
		public function check_email_exists($email_admin){
			$this->form_validation->set_message('check_email_exists', 'Email yang anda masukkan telah digunakan!');
			if($this->mregister->check_email_exists($email_admin)){
				return true;
			} else {
				return false;
			}
		}

		// Check if email exists
		public function check_userid_exists($nik_admin){
			$this->form_validation->set_message('check_userid_exists', 'NIK yang anda masukkan telah digunakan!');
			if($this->mregister->check_userid_exists($nik_admin)){
				return true;
			} else {
				return false;
			}
		}



		/*anggota*/
		public function indexanggota()
	{
		$data['title']='Registrasi';

		$this->form_validation->set_rules('nik_anggota', 'NIK','required|numeric|callback_check_userid_existsanggota');
		$this->form_validation->set_rules('username', 'Username', 'required|callback_check_username_existsanggota');
		$this->form_validation->set_rules('email_anggota', 'Email', 'required|callback_check_email_existsanggota');
		$this->form_validation->set_rules('password1', 'Password', 'required|min_length[4]');
		$this->form_validation->set_rules('password2', 'Confirm Password', 'matches[password1]');
	
		if($this->form_validation->run() === FALSE){
				$this->load->view('homeuser.php');
			} else {

				// Encrypt password
				$enc_password = md5($this->input->post('password1'));

				$this->mregister->registeranggota($enc_password);

				// Set set_message
				$this->session->set_flashdata('user_masuk', 'You are now registered and can log in');
				redirect(base_url("welcome"));
			}
	}

	// Check if username existsanggota
		public function check_username_existsanggota($username){
			$this->form_validation->set_message('check_username_existsanggota', 'Userneme yang anda masukkan telah digunakan!');
			if($this->mregister->check_username_existsanggota($username)){
				return true;
			} else {
				return false;
			}
		}

		// Check if email existsanggota
		public function check_email_existsanggota($email_anggota){
			$this->form_validation->set_message('check_email_existsanggota', 'Email yang anda masukkan telah digunakan!');
			if($this->mregister->check_email_existsanggota($email_anggota)){
				return true;
			} else {
				return false;
			}
		}

		// Check if email existsanggota
		public function check_userid_existsanggota($nik_anggota){
			$this->form_validation->set_message('check_userid_existsanggota', 'NIK yang anda masukkan telah digunakan!');
			if($this->mregister->check_userid_existsanggota($nik_anggota)){
				return true;
			} else {
				return false;
			}
		}

}
