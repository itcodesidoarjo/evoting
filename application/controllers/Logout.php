 <?php
class Logout extends CI_Controller{

  public function index(){
  	   if($this->session->userdata('logged_in')) : 
			$id = $this->session->userdata('ses_id_admin');
  	   	  	$data = [
	           'log_statusadmin' => 0,
	        ];
	      
	      	$where = array(
	          'id_admin' => $id
	      	);

  	   	  	$this->mregister->upedit_ver($data,$where);
      
	      	$this->session->sess_destroy();
	      	$this->session->set_flashdata('msg', 'Anda berhasil logout.');
	      	redirect(base_url('login'));
	    endif;
	    if($this->session->userdata('logged_anggota')) : 
	    	// $id = $this->session->userdata('ses_id_anggota');
  	   // 	  	$data = [
	     //       'log_statusadmin' => 0,
	     //    ];
	      
	     //  	$where = array(
	     //      'id_anggota' => $id
	     //  	);

  	   // 	  	$this->manggota->upedit_ver($data,$where);

	      	$this->session->sess_destroy();
	      	$this->session->set_flashdata('msg', 'Anda berhasil logout.');
	      	redirect(base_url('welcome'));
	    endif;
    }

    

}
?>