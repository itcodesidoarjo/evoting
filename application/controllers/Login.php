 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */
    public function __construct(){
        parent::__construct();
        if($this->session->userdata('status') == "login"){
            redirect(base_url("welcome"));
        }
    }
    public function index()
    {
      $this->load->helper('captcha');
 
      $vals = array(
          'img_path'   => './captcha/',
          'img_url'  => base_url().'captcha/',
          'img_width'  => '200',
          'img_height' => 30,
          'border' => 0, 
          'expiration' => 7200
      );

      // create captcha image
      $cap = create_captcha($vals);
      // store image html code in a variable
      $data['image'] = $cap['image'];
      // store the captcha word in a session
      $this->session->set_userdata('mycaptcha', $cap['word']);

      $this->load->view('login.php', $data);
    }
    
    public function auth()
    {   // if form was submitted and given captcha word matches one in session
      if (($this->input->post('secutity_code') == $this->session->userdata('mycaptcha'))) {
          
          $username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
          $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);

          $cek_login=$this->mlogin->auth_login($username,$password);
          if($cek_login->num_rows() > 0){ //jika login sebagai dosen
                $data=$cek_login->row_array();
                $this->session->set_flashdata('user_loggedin', 'Login berhasil.');
                      $data_session = array(
                     'status' => "login",
                     'logged_in' => true
                       );
                      $this->session->set_userdata('akses','1');
                      $this->session->set_userdata('ses_id_admin',$data['id_admin']);
                      $this->session->set_userdata('ses_nik_admin',$data['nik_admin']);
                      $this->session->set_userdata('ses_nama_admin',$data['nama_admin']);
                      $this->session->set_userdata('ses_telepon_admin',$data['telepon_admin']);
                      $this->session->set_userdata('ses_username',$data['username']);
                      $this->session->set_userdata('ses_email_admin',$data['email_admin']);
                      $this->session->set_userdata('ses_alamat_admin',$data['alamat_admin']);
                      $this->session->set_userdata('ses_foto_admin',$data['foto_admin']);
                      $this->session->set_userdata('ses_jk_admin',$data['jk_admin']);
                      $this->session->set_userdata('ses_tempatlahir_admin',$data['tempatlahir_admin']);
                      $this->session->set_userdata('ses_tgllahir_admin',$data['tgllahir_admin']);
                      $this->session->set_userdata('ses_tglreg_admin',$data['tglreg_admin']);
                      $this->session->set_userdata('ses_ver',0);
                      $this->session->set_userdata($data_session);

                      /*kode*/
                      $id_akun  = $this->session->userdata('ses_id_admin');
                      $mobile   = $this->session->userdata('ses_telepon_admin');

                      /*update anggota untuk insert kode*/
                      $i = date("i");
                      $s = date("s");
                      $data = [
                         'log_veradmin'   => $s.''.$id_akun.''.$i,
                         'log_statusadmin' => 0,
                      ];
                      
                        $where = array(
                          'id_admin' => $id_akun
                        );

                        $this->mregister->upedit_ver($data,$where); 

                        /*sms*/
                        $mess       = $message.' KODE VERIFIKASI LOGIN: "'.$s.''.$id_akun.''.$i.'"';

                        $msgencode = urlencode($mess);
                        $userkey = "n3gru9";
                        $passkey = "287njpeabx";
                        $router = "";

                        $postdata = array('authkey'=>$userkey,
                                  'mobile'=>$mobile,
                                  'message'=>$msgencode,
                                  'router'=>$router
                                  );
                        $url = "https://reguler.zenziva.net/apps/smsapi.php?userkey=$userkey&passkey=$passkey&nohp=$mobile&pesan=$msgencode";;

                        $ch  = curl_init();
                            curl_setopt_array($ch,array(
                                        CURLOPT_URL => $url,
                                        CURLOPT_RETURNTRANSFER => TRUE,
                                        CURLOPT_POST => TRUE,
                                        CURLOPT_POSTFIELDS => $postdata
                              ));

                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                        $output = curl_exec($ch);
                        if (curl_errno($ch)) {
                          echo "error". curl_error($ch);
                        }
                        curl_close($ch);
                      /*behasil*/
                      $this->session->set_flashdata('pesan', 'Berhasil Login.');
                      redirect(base_url("welcome/verifikasi"));
                   
            }else{
                 $this->session->set_flashdata('error', 'Username dan password yang anda masukkan salah.');
                 redirect(base_url("login"));
            }

        }else{
            $this->session->set_flashdata('error', 'Captcha yang anda masukkan salah.');
            redirect(base_url("login"));
        }
    }

     public function authanggota()
    {
      if (($this->input->post('secutity_code') == $this->session->userdata('mycaptcha'))) {

        $username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
        $cek_login=$this->mlogin->auth_loginanggota($username,$password);
        if($cek_login->num_rows() > 0){ //jika login sebagai dosen
              $data=$cek_login->row_array();
              $this->session->set_flashdata('user_loggedin', 'Login berhasil.');
                    $data_session = array(
                   'status' => "login",
                   'logged_anggota' => true
                     );
                    $this->session->set_userdata('akses','2');
                    $this->session->set_userdata('ses_id_anggota',$data['id_anggota']);
                    $this->session->set_userdata('ses_nik_anggota',$data['nik_anggota']);
                    $this->session->set_userdata('ses_nama_anggota',$data['nama_anggota']);
                    $this->session->set_userdata('ses_telepon_anggota',$data['telepon_anggota']);
                    $this->session->set_userdata('ses_username',$data['username']);
                    $this->session->set_userdata('ses_email_anggota',$data['email_anggota']);
                    $this->session->set_userdata('ses_alamat_anggota',$data['alamat_anggota']);
                    $this->session->set_userdata('ses_foto_anggota',$data['foto_anggota']);
                    $this->session->set_userdata('ses_tglreg_anggota',$data['tglreg_anggota']);
                    $this->session->set_userdata('ses_foto_anggota',$data['bidang_anggota']);
                    $this->session->set_userdata('ses_tglreg_anggota',$data['world_anggota']);
                    $this->session->set_userdata('ses_ver',0);
                    $this->session->set_userdata($data_session);

                    $this->session->set_flashdata('pesan', 'Berhasil Login.');
                    redirect(base_url("welcome"));
                 
          }else{
               $this->session->set_flashdata('error', 'Username dan password yang anda masukkan salah.');
               redirect(base_url("welcome/#about"));
          }
       }else{
            $this->session->set_flashdata('error', 'Captcha yang anda masukkan salah.');
            redirect(base_url("welcome/#about"));

        }
    }
}