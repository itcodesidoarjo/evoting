<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if($this->session->userdata('status') == "login"){
			$data['totalvote'] = $this->mregister->totalvote();
			$data['totalanggota'] = $this->mregister->totalanggota();
			$data['totalkekurangan'] = $this->mregister->totalkekurangan();
			$data['totalkandidat'] = $this->mregister->totalkandidat();
			$data['datagrafik'] = $this->mregister->datagrafik();
			$data['vote'] = $this->mvote->data_vote();

            $this->load->view('header.php');
			$this->load->view('home.php',$data);
			$this->load->view('footer.php');
        }else{
        	  $this->load->helper('captcha');
 
		      $vals = array(
		          'img_path'   => './captcha/',
		          'img_url'  => base_url().'captcha/',
		          'img_width'  => '200',
		          'img_height' => 30,
		          'border' => 0, 
		          'expiration' => 7200
		      );

		      // create captcha image
		      $cap = create_captcha($vals);
		      // store image html code in a variable
		      $data['image'] = $cap['image'];
		      // store the captcha word in a session
		      $this->session->set_userdata('mycaptcha', $cap['word']);

        	$this->load->view('homeuser.php', $data);
        }
		
	}

	public function register()
	{
		$this->load->view('register.php');
	}

	public function verifikasi()
	{
            $this->load->view('header.php');
		$this->load->view('verifikasi.php');
			$this->load->view('footer.php');
	}

	public function verifikas_anggota()
	{
            $this->load->view('header.php');
		$this->load->view('vote/verifikasi_anggota.php');
			$this->load->view('footer.php');
	}
}
