<?php
	class Manggota extends CI_Model
	{

	public function input_anggota($data)
	{
	   $this->db->insert('tbl_anggota', $data);
	   $id = $this->db->insert_id();

	   // $mobile 	= $this->input->post('telepon_anggota');
	   // $message 	= "HARAP DI INGAT KODE INI, GUNAKAN SAAT LOGIN KEMBALI.";

	 //   	$t = date("is");
		// $data1 = [
		//    'log_veradmin'   => $id.''.$t,
		//    'log_statusadmin' => 0,
		// ];

		// $where = array(
	 //  		'id_anggota' => $id
		// );

		// $this->upedit_ver($data1,$where); 

			/*sms*/
		// $mess       = $message.' KODE VERIFIKASI LOGIN: "'.$id.''.$t.'"';

		// $msgencode = urlencode($mess);
		// $userkey = "n3gru9";
		// $passkey = "287njpeabx";
		// $router = "";

		// $postdata = array('authkey'=>$userkey,
		//           'mobile'=>$mobile,
		//           'message'=>$msgencode,
		//           'router'=>$router
		//           );
		// $url = "https://reguler.zenziva.net/apps/smsapi.php?userkey=$userkey&passkey=$passkey&nohp=$mobile&pesan=$msgencode";;

		// $ch  = curl_init();
		//     curl_setopt_array($ch,array(
		//                 CURLOPT_URL => $url,
		//                 CURLOPT_RETURNTRANSFER => TRUE,
		//                 CURLOPT_POST => TRUE,
		//                 CURLOPT_POSTFIELDS => $postdata
		//       ));

		// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		// $output = curl_exec($ch);
		// if (curl_errno($ch)) {
		//   echo "error". curl_error($ch);
		// }
		// 	curl_close($ch);
	}

	public function upedit_ver($data,$where){
	  	$this->db->where($where);
		$this->db->update('tbl_anggota',$data);
	} 

	public function data_anggota(){
    $query=$this->db->query("
    	SELECT * FROM tbl_anggota
    	");
   	 return $query->result();
	}

	 public function detail_anggota($id){
    $query=$this->db->query("
    	SELECT * FROM tbl_anggota where id_anggota = ".$id);
   	 return $query->result();
	}

	public function hapus_anggota($id)
	{
		$query=$this->db->query("
    		SELECT * FROM tbl_kandidat where id_anggota = ".$id
    	);
		if (empty($query->result())) {
			$this->db->delete('tbl_anggota', ['id_anggota' => $id]);
		}else{
			return $query->result();
		}
    	
	}

	public function edit_anggota($id){
    $query=$this->db->query("
    	SELECT * FROM tbl_anggota
    	where id_anggota = ".$id
    	);
   	 return $query->result();
	}

	public function upedit_anggota($data,$where){
	  	$this->db->where($where);
		$this->db->update('tbl_anggota',$data);
	} 

	public function nomer_anggota(){
    $query=$this->db->query("
    	SELECT telepon_anggota, id_anggota FROM tbl_anggota ");
   	 return $query->result_array();
	}

	public function cek_anggota($tlpn){
    $query=$this->db->query("
    	SELECT telepon_anggota, id_anggota FROM tbl_anggota 
    	where telepon_anggota = '".$tlpn."'");
   	 return $query->result_array();
	}

	public function cek_no($no){
    $query=$this->db->query("
    	SELECT telepon_anggota, id_anggota FROM tbl_anggota 
    	where nik_anggota = '".$no."'");
   	 return $query->result_array();
	}

	public function cek_anggotaup($tlpn,$id){
    $query=$this->db->query("
    	SELECT telepon_anggota, id_anggota FROM tbl_anggota 
    	where telepon_anggota = '".$tlpn."' AND id_anggota <> '".$id."'");
   	 return $query->result_array();
	}

	public function cek_noup($no,$id){
    $query=$this->db->query("
    	SELECT telepon_anggota, id_anggota FROM tbl_anggota 
    	where nik_anggota = '".$no."' AND id_anggota <> '".$id."'");
   	 return $query->result_array();
	}

}