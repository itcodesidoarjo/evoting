<?php
	class Mundangan extends CI_Model
	{

	 public function input_undangan($data)
	{
	   $this->db->insert('tbl_undangan', $data);
	}

	public function data_undangan(){
    $query=$this->db->query("
    	SELECT * FROM tbl_undangan");
   	 return $query->result();
	}

	public function input_ballot($data1)
	{
	   $this->db->insert('tbl_ballot', $data1);
	}

	public function cek_ballot($id_undangan,$id_kandidat){
    $query=$this->db->query("
    	SELECT * FROM tbl_ballot
    	WHERE TRUE AND id_undangan = ".$id_undangan." AND id_kandidat = ".$id_kandidat);
   	 return $query->result_array();
	}

	public function cek_ballotdua($id_undangan,$no_ballot,$level_ballot){
    $query=$this->db->query("
    	SELECT * FROM tbl_ballot
    	WHERE TRUE AND no_ballot = ".$no_ballot." AND level_ballot = '".$level_ballot."' AND id_undangan = ".$id_undangan);
   	 return $query->result_array();
	}

	public function data_vote(){
    $query=$this->db->query("SELECT a.id_ballot,a.no_ballot, b.tgl_undangan, b.tentang_undangan, d.nama_anggota, c.level_kandidat, c.periode_kandidat 
				FROM tbl_ballot a
				LEFT JOIN tbl_undangan b ON b.id_undangan = a.id_undangan
				LEFT JOIN tbl_kandidat c ON c.id_kandidat = a.id_kandidat
				INNER JOIN tbl_anggota d ON d.id_anggota = c.id_anggota
				ORDER BY b.tgl_undangan ASC, c.level_kandidat ASC, a.no_ballot ASC");
   	 return $query->result();
	}

	public function data_editvote($id){
    $query=$this->db->query("SELECT a.id_ballot,a.no_ballot, b.tgl_undangan, b.tentang_undangan, d.nama_anggota, c.level_kandidat ,
    			DATE_FORMAT(b.tgl_undangan, '%Y') AS tgl 
				FROM tbl_ballot a
				LEFT JOIN tbl_undangan b ON b.id_undangan = a.id_undangan
				LEFT JOIN tbl_kandidat c ON c.id_kandidat = a.id_kandidat
				INNER JOIN tbl_anggota d ON d.id_anggota = c.id_anggota
				where a.id_ballot =".$id);
   	 return $query->result();
	}

	public function notifikasi(){
	$anggota = (!empty($this->session->userdata('ses_id_anggota')))? "WHERE a.id_anggota <> ".$this->session->userdata('ses_id_anggota'):'';
    $query=$this->db->query("SELECT *
				FROM tbl_perhitungan a
				LEFT JOIN tbl_undangan b ON b.id_undangan = a.id_undangan
				".$anggota."
				GROUP BY a.id_undangan");
   	 return $query->result();
	}

	public function cek_tglundangan($newDate){
	    $query=$this->db->query("
	    	SELECT DATE_FORMAT(tgl_undangan, '%Y-%m-%d') AS tgl
			FROM tbl_undangan
			HAVING tgl = '".$newDate."'");
	   	 return $query->result();
	}

	public function cek_tanggalperiode($periode,$level){
	    $query=$this->db->query('
	    	SELECT periode_kandidat,id_kandidat
			FROM tbl_kandidat
			WHERE periode_kandidat = "'.$periode.'" AND level_kandidat = "'.$level.'"'
		);
	   	 return $query->result();
	}

	public function createcek_undangan($newDate){
	    $query=$this->db->query('
	    	SELECT DATE_FORMAT(tgl_undangan, "%Y-%m-%d") AS tgl, id_undangan
			FROM tbl_undangan
			HAVING tgl = "'.$newDate.'"'
		);
	   	 return $query->result();
	}

	public function cektahun_undangan($newDate){
	    $query=$this->db->query('
	    	SELECT DATE_FORMAT(tgl_undangan, "%Y") AS tgl, id_undangan
			FROM tbl_undangan
			HAVING tgl = "'.$newDate.'"'
		);
	   	 return $query->result();
	}

	 public function create_undangan($data)
	{
	   	$this->db->insert('tbl_undangan', $data);
	 	$id = $this->db->insert_id();
	   	
	   	return $id;
	}

	public function cekcreate_ballot($id_undangan,$level){
    $query=$this->db->query('
    	SELECT * FROM tbl_ballot
    	WHERE TRUE AND id_undangan = "'.$id_undangan.'" AND level_ballot = "'.$level.'"');
   	 return $query->result_array();
	}

	public function detail_undangan($id){
    $query=$this->db->query("SELECT a.*,b.*,c.*,d.* 
				FROM tbl_ballot a
				LEFT JOIN tbl_undangan b ON b.id_undangan = a.id_undangan
				LEFT JOIN tbl_kandidat c ON c.id_kandidat = a.id_kandidat
				INNER JOIN tbl_anggota d ON d.id_anggota = c.id_anggota
				where a.id_ballot =".$id);
   	 return $query->result();
	}

	public function up_ball($data,$where){
	  	$this->db->where($where);
		$this->db->update('tbl_ballot',$data);
	} 

	public function cek_ball($no_ballot,$level_kandidat,$tgl){
	    $query=$this->db->query('
	    	SELECT a.id_ballot,a.no_ballot, b.tgl_undangan, b.tentang_undangan, d.nama_anggota, c.level_kandidat ,
    			DATE_FORMAT(b.tgl_undangan, "%Y") AS tgl 
				FROM tbl_ballot a
				LEFT JOIN tbl_undangan b ON b.id_undangan = a.id_undangan
				LEFT JOIN tbl_kandidat c ON c.id_kandidat = a.id_kandidat
				INNER JOIN tbl_anggota d ON d.id_anggota = c.id_anggota
				where TRUE AND a.no_ballot ="'.$no_ballot.'" AND c.level_kandidat ="'.$level_kandidat.'"
				HAVING tgl ="'.$tgl.'"');
	   	 return $query->result();
	}

}