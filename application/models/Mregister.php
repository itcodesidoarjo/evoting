<?php
	class Mregister extends CI_Model
	{
		public function register($enc_password){
			// User data array
			date_default_timezone_set('Asia/Jakarta');
			$mobile 	= $this->input->post('telepon_admin');
			$message 	= "HARAP DI INGAT KODE INI, GUNAKAN SAAT LOGIN KEMBALI.";

			$dat_user = array(
				'nik_admin'		=> $this->input->post('nik_admin'),
				'username'		=> $this->input->post('username'),
				'telepon_admin'	=> $this->input->post('telepon_admin'),
                'email_admin' 	=> $this->input->post('email_admin'),
                'password' 		=> $enc_password,
                'tglreg_admin'=> date("Y-m-d h:i:sa"),
			);
			/*-------Mengambil id users dan mengirimkan ke model-----*/
			$id_akun = $this->tambah_akun($dat_user);
			/*-----------Menangkap Data Dari Form------------------*/
			/*update anggota untuk insert kode*/
			$t = date("is");
	        $data = [
	           'log_veradmin'   => $id_akun.''.$t,
	           'log_statusadmin' => 0,
	        ];
          
          	$where = array(
              'id_admin' => $id_akun
          	);

          	$this->upedit_ver($data,$where); 

          	/*sms*/
            $mess       = $message.' KODE VERIFIKASI LOGIN: "'.$id_akun.''.$t.'"';

            $msgencode = urlencode($mess);
            $userkey = "n3gru9";
            $passkey = "287njpeabx";
            $router = "";

            $postdata = array('authkey'=>$userkey,
                      'mobile'=>$mobile,
                      'message'=>$msgencode,
                      'router'=>$router
                      );
            $url = "https://reguler.zenziva.net/apps/smsapi.php?userkey=$userkey&passkey=$passkey&nohp=$mobile&pesan=$msgencode";;

            $ch  = curl_init();
                curl_setopt_array($ch,array(
                            CURLOPT_URL => $url,
                            CURLOPT_RETURNTRANSFER => TRUE,
                            CURLOPT_POST => TRUE,
                            CURLOPT_POSTFIELDS => $postdata
                  ));

            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            $output = curl_exec($ch);
            if (curl_errno($ch)) {
              echo "error". curl_error($ch);
            }
          	curl_close($ch);
	        
          	/*sukses*/
		  	$this->session->set_flashdata('user_masuk', 'Registrasi berhasil. Silahkan Login disini!.');
		  	redirect('login'); //mengembalikan halaman setelah berhasil menginputkan data
		}
		
		public function upedit_ver($data,$where){
		  	$this->db->where($where);
			$this->db->update('tbl_admin',$data);
		} 

		public function tambah_akun($data)
		   {
		       $this->db->insert('tbl_admin', $data);
		       $id = $this->db->insert_id();
		       return (isset($id)) ? $id : FALSE;
		   }


		// register ===================================================chek data=======================================
		public function check_username_exists($username){
			$query = $this->db->get_where('tbl_admin', array('username' => $username));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}

	
		public function check_email_exists($email_admin){
			$query = $this->db->get_where('tbl_admin', array('email_admin' => $email_admin));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}

		
		public function check_userid_exists($nik_admin){
			$query = $this->db->get_where('tbl_admin', array('nik_admin' => $nik_admin));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}

		/*anggota*/
		public function registeranggota($enc_password){
			
			date_default_timezone_set('Asia/Jakarta');
			$dat_user = array(
				'nik_anggota'		=> $this->input->post('nik_anggota'),
				'username'		=> $this->input->post('username'),
                'email_anggota' 	=> $this->input->post('email_anggota'),
                'password' 		=> $enc_password,
                'tglreg_anggota'=> date("Y-m-d h:i:sa"),
			);
			$id_akun = $this->tambah_akunanggota($dat_user);
			
		  	$this->session->set_flashdata('user_masuk', 'Registrasi berhasil. Silahkan Login disini!.');
		  	redirect('welcome'); 
		}
		

		public function tambah_akunanggota($data)
		   {
		       $this->db->insert('tbl_anggota', $data);
		       $id = $this->db->insert_id();
		       return (isset($id)) ? $id : FALSE;
		   }


		public function check_username_existsanggota($username){
			$query = $this->db->get_where('tbl_anggota', array('username' => $username));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}

	
		public function check_email_existsanggota($email_anggota){
			$query = $this->db->get_where('tbl_anggota', array('email_anggota' => $email_anggota));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}

		
		public function check_userid_existsanggota($nik_anggota){
			$query = $this->db->get_where('tbl_anggota', array('nik_anggota' => $nik_anggota));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}

		public function totalvote(){
			date_default_timezone_set('Asia/Jakarta');
			$tgl 	= date("Y-m-d");

		    $query=$this->db->query("SELECT COUNT(id_perhitungan) AS totalhitung
					FROM(
					SELECT id_perhitungan ,DATE_FORMAT(tglreg_perhitungan, '%Y-%m-%d') AS tgl
					FROM tbl_perhitungan 
					GROUP BY id_anggota
					HAVING tgl = '".$tgl."'
					)a"
		    	);
		   	 return $query->result();
		}

		public function totalanggota(){
		    $query=$this->db->query("SELECT COUNT(id_anggota) AS totalanggota
				FROM(
				SELECT * FROM tbl_anggota 
				)a");
		   	 return $query->result();
		}
		public function totalkekurangan(){
			
			date_default_timezone_set('Asia/Jakarta');
			$tgl 	= date("Y-m-d");
		    $query=$this->db->query("SELECT (SUM(totalanggota) -SUM(totalperhitungan)) AS tot
					FROM
					(SELECT COUNT(id_anggota) AS totalanggota, (0) AS totalperhitungan
					FROM(
					SELECT * FROM tbl_anggota a
					)a
					UNION
					SELECT (0)AS totalanggota, COUNT(id_perhitungan) AS totalperhitungan
					FROM(
					SELECT *,DATE_FORMAT(tglreg_perhitungan, '%Y-%m-%d') AS tgl FROM tbl_perhitungan a
					GROUP BY id_anggota
					HAVING tgl = '".$tgl."'
					)a
			)a");
		   	 return $query->result();
		}

		public function totalkandidat(){
		    $query=$this->db->query("SELECT COUNT(a.id_kandidat) AS totalkandidat 
				FROM(SELECT a.id_kandidat
				FROM tbl_kandidat a
				LEFT JOIN tbl_anggota b ON a.id_anggota = b.id_anggota
				WHERE TRUE AND a.id_anggota IS NOT NULL 
				)a	");
		 return $query->result();
		}

		public function datagrafik(){
			date_default_timezone_set('Asia/Jakarta');
			$tgl 	= date("Y-m-d");

		    $query=$this->db->query('SELECT SUM(suara_spgpram) AS jmlpram,SUM(suara_spvcoor) AS jmlspv,SUM(suara_csokasir) AS jmlkasir, nama_anggota, level_ballot, tgl,no_ballot
				FROM
				(SELECT   d.id_anggota,b.no_ballot, a.suara_spgpram,a.suara_csokasir,a.suara_spvcoor, d.nama_anggota,b.`level_ballot`, DATE_FORMAT(tglreg_perhitungan, "%Y-%m-%d") AS tgl
				FROM tbl_perhitungan a
				LEFT JOIN tbl_ballot b ON b.id_ballot=a.id_ballot
				LEFT JOIN tbl_kandidat c ON c.id_kandidat=b.id_kandidat
				LEFT JOIN tbl_anggota d ON d.id_anggota= c.id_anggota
				HAVING tgl = "'.$tgl.'" )a GROUP BY a.id_anggota ORDER BY level_ballot ASC, no_ballot ASC');
		 return $query->result();
		}
		
}