<?php
	class Mvote extends CI_Model
	{
		public function data_vote(){
			date_default_timezone_set('Asia/Jakarta');
		   	$tgl 	=date("Y-m-d");

		    $query=$this->db->query('SELECT b.id_ballot,a.id_undangan, b.id_kandidat, b.no_ballot,d.nama_anggota,d.nik_anggota, d.foto_anggota, c.visi_kandidat, c.misi_kandidat, c.level_kandidat 
				, a.level_undangan, a.tgl_undangan, b.level_ballot
				FROM tbl_undangan a
				LEFT JOIN tbl_ballot b ON b.id_undangan = a.id_undangan
				LEFT JOIN tbl_kandidat c ON c.id_kandidat = b.id_kandidat
				LEFT JOIN tbl_anggota d ON d.id_anggota = c.id_anggota
				WHERE TRUE AND a.tgl_undangan = "'.$tgl.'"');
		   	 return $query->result();
		}

		public function data_votepram(){
			date_default_timezone_set('Asia/Jakarta');
		   	$tgl 	=date("Y-m-d");

		    $query=$this->db->query('SELECT b.id_ballot,a.id_undangan, b.id_kandidat, b.no_ballot,d.nama_anggota,d.nik_anggota, d.foto_anggota, c.visi_kandidat, c.misi_kandidat, c.level_kandidat 
				, a.level_undangan, a.tgl_undangan, b.level_ballot
				FROM tbl_undangan a
				LEFT JOIN tbl_ballot b ON b.id_undangan = a.id_undangan
				LEFT JOIN tbl_kandidat c ON c.id_kandidat = b.id_kandidat
				LEFT JOIN tbl_anggota d ON d.id_anggota = c.id_anggota
				WHERE TRUE AND b.level_ballot = "SPG PRAM" AND a.tgl_undangan = "'.$tgl.'"');
		   	 return $query->result();
		}

		public function data_votekasir(){
			date_default_timezone_set('Asia/Jakarta');
		   	$tgl 	=date("Y-m-d");

		    $query=$this->db->query('SELECT b.id_ballot,a.id_undangan, b.id_kandidat, b.no_ballot,d.nama_anggota,d.nik_anggota, d.foto_anggota, c.visi_kandidat, c.misi_kandidat, c.level_kandidat 
				, a.level_undangan, a.tgl_undangan, b.level_ballot
				FROM tbl_undangan a
				LEFT JOIN tbl_ballot b ON b.id_undangan = a.id_undangan
				LEFT JOIN tbl_kandidat c ON c.id_kandidat = b.id_kandidat
				LEFT JOIN tbl_anggota d ON d.id_anggota = c.id_anggota
				WHERE TRUE AND b.level_ballot = "CSO KASIR" AND a.tgl_undangan = "'.$tgl.'"');
		   	 return $query->result();
		}

		public function data_votespv(){
			date_default_timezone_set('Asia/Jakarta');
		   	$tgl 	=date("Y-m-d");

		    $query=$this->db->query('SELECT b.id_ballot,a.id_undangan, b.id_kandidat, b.no_ballot,d.nama_anggota,d.nik_anggota, d.foto_anggota, c.visi_kandidat, c.misi_kandidat, c.level_kandidat 
				, a.level_undangan, a.tgl_undangan, b.level_ballot
				FROM tbl_undangan a
				LEFT JOIN tbl_ballot b ON b.id_undangan = a.id_undangan
				LEFT JOIN tbl_kandidat c ON c.id_kandidat = b.id_kandidat
				LEFT JOIN tbl_anggota d ON d.id_anggota = c.id_anggota
				WHERE TRUE AND b.level_ballot = "SPV COOR" AND a.tgl_undangan = "'.$tgl.'"');
		   	 return $query->result();
		}


		// public function votedata($id){
	 //    $query=$this->db->query("SELECT b.id_ballot,a.id_undangan, b.id_kandidat, b.no_ballot,d.nama_anggota,d.nik_anggota, d.foto_anggota, c.visi_kandidat, c.misi_kandidat, c.level_kandidat , a.level_undangan
		// 		FROM tbl_undangan a
		// 		LEFT JOIN tbl_ballot b ON b.id_undangan = a.id_undangan
		// 		LEFT JOIN tbl_kandidat c ON c.id_kandidat = b.id_kandidat
		// 		LEFT JOIN tbl_anggota d ON d.id_anggota = c.id_anggota
		// 		WHERE a.id_undangan =".$id);
	 //   	 return $query->result();
		// }

		 public function input_vote($data)
		{
		   $this->db->insert('tbl_perhitungan', $data);
		}


		// public function cekvote($id,$anggota){
	 //    $query=$this->db->query("SELECT*
		// 		FROM tbl_perhitungan 
		// 		WHERE id_anggota = ".$anggota." AND id_undangan =".$id."
		// 		LIMIT 1");
	 //   	 return $query->result();
		// }

		public function kodevote(){
	    $query=$this->db->query('SELECT YEAR(tgl_undangan) AS th, MONTH(tgl_undangan) AS bln, CONCAT(YEAR(tgl_undangan),"V",MONTH(tgl_undangan)) AS kode
				FROM tbl_undangan
				ORDER BY tgl_undangan DESC
				LIMIT 1');
	   	 return $query->result();
	   	}

	   	public function update_verifikasi($data,$where){
		  	$this->db->where($where);
			$this->db->update('tbl_anggota',$data);
		} 

		public function update_verifikasiadmin($data,$where){
		  	$this->db->where($where);
			$this->db->update('tbl_admin',$data);
		} 

		public function cekvery($id){
	    $query=$this->db->query('SELECT *
				FROM tbl_anggota
				WHERE TRUE AND id_anggota = "'.$id.'"');
	   	 return $query->result();
	   	}

	   	public function hitungpram($id){
	   	date_default_timezone_set('Asia/Jakarta');
		$tgl 	=date("Y-m-d");

	    $query=$this->db->query('SELECT *, DATE_FORMAT(tglreg_perhitungan, "%Y-%m-%d") AS tgl
			FROM tbl_perhitungan
			WHERE TRUE AND suara_spgpram = 1 AND suara_csokasir = "" AND suara_spvcoor = "" AND id_anggota = "'.$id.'"
			HAVING tgl = "'.$tgl.'"');
	   	 return $query->result();
	   	}

	   	public function hitungkasir($id){
	   	date_default_timezone_set('Asia/Jakarta');
		$tgl 	=date("Y-m-d");

	    $query=$this->db->query('SELECT *, DATE_FORMAT(tglreg_perhitungan, "%Y-%m-%d") AS tgl
			FROM tbl_perhitungan
			WHERE TRUE AND suara_spgpram = "" AND suara_csokasir = 1 AND suara_spvcoor = "" AND id_anggota = "'.$id.'"
			HAVING tgl = "'.$tgl.'"');
	   	 return $query->result();
	   	}

	   	public function hitungspv($id){
	   	date_default_timezone_set('Asia/Jakarta');
		$tgl 	=date("Y-m-d");

	    $query=$this->db->query('SELECT *, DATE_FORMAT(tglreg_perhitungan, "%Y-%m-%d") AS tgl
			FROM tbl_perhitungan
			WHERE TRUE AND suara_spgpram = "" AND suara_csokasir = "" AND suara_spvcoor = 1 AND id_anggota = "'.$id.'"
			HAVING tgl = "'.$tgl.'"');
	   	 return $query->result();
	   	}

	   	public function datavotecetak(){
	    $query=$this->db->query('SELECT a.id_undangan, tgl_undangan, keteranagan_undangan, no_ballot, level_ballot, nama_anggota, 			spg,cso,spv  
				FROM tbl_undangan a
				LEFT JOIN tbl_ballot b ON b.id_undangan = a.id_undangan
				LEFT JOIN tbl_kandidat c ON c.id_kandidat = b.id_kandidat
				LEFT JOIN tbl_anggota d ON d.id_anggota = c.id_anggota 
				LEFT JOIN (
					SELECT SUM(suara_spgpram) AS spg , SUM(suara_csokasir) AS cso,SUM(suara_spvcoor) AS spv, id_ballot, suara_spgpram, suara_csokasir, suara_spvcoor FROM
						tbl_perhitungan
						GROUP BY id_ballot
					)e ON e.id_ballot = b.id_ballot 
				ORDER BY tgl_undangan ASC , level_ballot ASC, no_ballot ASC
		');
		
	   	 return $query->result();
	   	}

	   	public function verlog($data){
	    $query=$this->db->query('SELECT *
				FROM tbl_anggota
				WHERE TRUE AND log_veradmin = "'.$data.'"');
	   	 return $query->result();
	   	}
	   	public function verlogadmin($data){
	    $query=$this->db->query('SELECT *
				FROM tbl_admin
				WHERE TRUE AND log_veradmin = "'.$data.'"');
	   	 return $query->result();
	   	}

	   	public function cek_ver($id){
	    $query=$this->db->query('SELECT *
				FROM tbl_anggota
				WHERE TRUE AND id_anggota = "'.$id.'" AND statusver_anggota = 0');
	   	 return $query->result();
	   	}

	   	public function cek_inputver($kode,$id){
	    $query=$this->db->query('SELECT *
				FROM tbl_anggota
				WHERE TRUE AND id_anggota = "'.$id.'" AND kodever_anggota = "'.$kode.'"');
	   	 return $query->result();
	   	}
	}
