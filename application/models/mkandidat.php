<?php
	class Mkandidat extends CI_Model
	{

	 public function input_kandidat($data)
	{

	   $this->db->insert('tbl_kandidat', $data);
	}

	public function data_kandidat(){
    $query=$this->db->query("
    	SELECT * 
		FROM tbl_kandidat a
		INNER JOIN tbl_anggota b ON a.id_anggota = b.id_anggota
		where TRUE AND a.id_anggota IS NOT NULL 	");
   	 return $query->result();
	}

	public function cekdata_kandidat($id,$periode,$level){
    $query=$this->db->query('
    	SELECT * 
		FROM tbl_kandidat
		WHERE TRUE AND periode_kandidat = "'.$periode.'"
		AND id_anggota = '.$id.' 
		AND level_kandidat = "'.$level.'"');
   	 return $query->result();
	}

	 public function detail_kandidat($id){
    $query=$this->db->query("
    	SELECT * 
		FROM tbl_kandidat a
		INNER JOIN tbl_anggota b ON a.id_anggota = b.id_anggota where a.id_kandidat = ".$id);
   	 return $query->result();
	}

	public function hapus_kandidat($id)
	{
		$query=$this->db->query("
    		SELECT * FROM tbl_ballot where id_kandidat = ".$id
    	);
    	if (empty($query->result())) {
			$this->db->delete('tbl_kandidat', ['id_kandidat' => $id]);
		}else{
			return $query->result();
		}
		
	}

	 public function edit_kandidat($id){
    $query=$this->db->query("
    	SELECT * 
		FROM tbl_kandidat a
		INNER JOIN tbl_anggota b ON a.id_anggota = b.id_anggota where a.id_kandidat  = ".$id);
   	 return $query->result();
	}

	
	public function upedit_kandidat($data,$where){
	  	$this->db->where($where);
		$this->db->update('tbl_kandidat',$data);
	} 

	public function cek_bidang1($id,$bidang_kandidat,$level){
    $query=$this->db->query('
		SELECT "SPG PRAM" AS nama
			FROM tbl_anggota
			HAVING nama LIKE "%'.$bidang_kandidat.'%"
			LIMIT 1
		');
   	 return $query->result();
	}

	public function cek_bidang2($id,$bidang_kandidat,$level){
    $query=$this->db->query('
		SELECT "CSO KASIR" AS nama
			FROM tbl_anggota
			HAVING nama LIKE "%'.$bidang_kandidat.'%"
			LIMIT 1
		');
   	 return $query->result();
	}

	public function cek_bidang3($id,$bidang_kandidat,$level){
    $query=$this->db->query('
		SELECT "SPV COOR" AS nama
			FROM tbl_anggota
			HAVING nama LIKE "%'.$bidang_kandidat.'%"
			LIMIT 1
		');
   	 return $query->result();
	}
}