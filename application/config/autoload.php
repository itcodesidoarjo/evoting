<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$autoload['packages'] = array();


$autoload['libraries'] = array('database','session','email','table','form_validation','pagination');


$autoload['drivers'] = array();


$autoload['helper'] = array('url','form','text','date','security','url_helper','captcha');


$autoload['config'] = array();


$autoload['language'] = array();


$autoload['model'] = array('mregister','mlogin','manggota','mkandidat','mundangan','mvote');
