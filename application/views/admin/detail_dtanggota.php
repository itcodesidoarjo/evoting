
            <!-- Mobile Menu end -->
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list single-page-breadcome">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="breadcome-heading">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Profil Details</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Single pro tab start-->
        <?php foreach ($data_anggota as $a) {
        
         ?>
        <div class="single-product-tab-area mg-t-15 mg-b-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <div id="myTabContent1" class="tab-content">
                            <div class="product-tab-list tab-pane fade active in" id="single-tab1">
                                <img src="<?php echo base_url() . 'asset/img/'.$a->foto_anggota?>" alt="" />
                            </div>
                        </div>
                       
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                        <div class="single-product-details res-pro-tb">
                            <div class="color-quality-pro">
                                <div class="clear"></div>
                                <div class="single-social-area">
                                    <h3>No Identitas     :<?php echo $a->nik_anggota?></h3>
                                    <h3>Nama    : <?php echo $a->nama_anggota ?></h3>
                                    <h3>Bidang  : <?php echo $a->bidang_anggota ?></h3>
                                    <h3>World   : <?php echo $a->world_anggota ?></h3>
                                    <h3>Brand   : <?php echo $a->brand_anggota ?></h3>
                                    <h3>Telepon : <?php echo $a->telepon_anggota ?></h3>
                                    <h3>Email   : <?php echo $a->email_anggota ?></h3>
                                </div>
                            </div>
                            <div class="single-pro-cn">
                                <h3>ALAMAT</h3>
                                <p><?php echo $a->alamat_anggota ?></p>
                            </div>
                                <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   
       