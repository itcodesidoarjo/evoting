<div class="review-tab-pro-inner">
    <ul id="myTab3" class="tab-review-design">
    <?php if($this->session->flashdata('pesan')): ?>
            <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                    </button>
                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
            </div>
        <?php endif; ?>
         <?php if($this->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                    </button>
                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
            </div>
        <?php endif; ?>
    <li class="active"><a href="#description"><i class="fa fa-pencil" aria-hidden="true"></i>Tambah Data Anggota</a></li>
    </ul>
        <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
                <div class="row">
                    <?php echo form_open_multipart('anggota/inputanggota'); ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="review-content-section">
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                <input type="text" name="nik_anggota" class="form-control" required="" placeholder="No. Identitas">
                            </div>
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <input type="text" name="nama_anggota" class="form-control" required="" placeholder="Nama Anggota">
                            </div>
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                <input type="text" name="bidang_anggota" class="form-control" required="" placeholder="Bidang">
                            </div>
                           <!--   <select name="bidang_anggota" required="" class="form-control pro-edt-select form-control-primary">
                                <option value="" > Pilih Bidang</option>
                                <option value="SPG PRAM">SPG PRAM</option>
                                <option value="CSO KASIR">CSO KASIR</option>
                                <option value="SPV COOR">SPV COOR</option>
                            </select> -->
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                <input type="text" name="world_anggota" class="form-control" placeholder="World">
                            </div>
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-file-image-o" aria-hidden="true"></i></span>
                                <input type="file" name="userfile" class="form-control" required="" placeholder="Foto">
                            </div>

                             <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-home" aria-hidden="true"></i></span>
                                <textarea class="form-control" name="alamat_anggota" placeholder="Alamat"></textarea> 
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="review-content-section">
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
                                <input type="text" name="brand_anggota" class="form-control" placeholder="Brand">
                            </div>
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                <input type="text" name="telepon_anggota" class="form-control" required="" placeholder=" Telepon">
                            </div>
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <input type="text" name="username" class="form-control" placeholder="Username">
                            </div>
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
                                <input type="password" name="password" class="form-control" required="" placeholder="Password">
                            </div>
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                <input type="email" name="email_anggota" class="form-control" required="" placeholder="Email">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="text-center mg-b-pro-edt custom-pro-edt-ds">
                            <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Simpan
                                </button>
                            <a href="<?php echo base_url() ?>anggota" type="button" class="btn btn-warning waves-effect waves-light">Batal
                                </a>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
</div>
