   
                         <!-- <li>
                            <a class="has-arrow" href="<?php echo base_url() ?>index.php/Admin/data_vote" aria-expanded="false"><i class="fa big-icon fa-desktop icon-wrap"></i> <span class="mini-click-non">Report Vote</span></a>
                        </li> -->
                </nav>
            </div>
        </nav>
    </div>
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="index.html"><img class="main-logo" src="<?php echo base_url() ?>asset/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row">
                                    <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                        <div class="menu-switcher-pro">
                                            <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                                    <i class="fa fa-bars"></i>
                                                </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
                                        <div class="header-top-menu tabl-d-n">
                                            
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                        <div class="header-right-info">
                                            <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                                <li class="nav-item">
                                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                            <i class="fa fa-user adminpro-user-rounded header-riht-inf" aria-hidden="true"></i>
                                                            <span class="admin-name">Advanda Cro</span>
                                                            <i class="fa fa-angle-down adminpro-icon adminpro-down-arrow"></i>
                                                        </a>
                                                    <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                                                        <li><a href="register.html"><span class="fa fa-home author-log-ic"></span>Register</a>
                                                        </li>
                                                        <li><a href="#"><span class="fa fa-user author-log-ic"></span>My Profile</a>
                                                        </li>
                                                        <li><a href="lock.html"><span class="fa fa-diamond author-log-ic"></span> Lock</a>
                                                        </li>
                                                        <li><a href="#"><span class="fa fa-cog author-log-ic"></span>Settings</a>
                                                        </li>
                                                        <li><a href="login.html"><span class="fa fa-lock author-log-ic"></span>Log Out</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul class="mobile-menu-nav">
                                        <li><a data-toggle="collapse" data-target="#Charts" href="#">Dashboard <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#demo" href="#">Vote <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>                                        </li>
                                        <li><a data-toggle="collapse" data-target="#others" href="#">Data Kandidat <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                            <ul id="others" class="collapse dropdown-header-top">
                                                <li><a href="file-manager.html">SPV & Kordinator</a></li>
                                                <li><a href="contacts.html">Kasir & CSO</a></li>
                                                <li><a href="projects.html">SPG & Pram</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu end -->
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="breadcome-heading">
                                            <form role="search" class="">
                                                <input type="text" placeholder="Search..." class="form-control">
                                                <a href=""><i class="fa fa-search"></i></a>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="#"></a> <span class="bread-slash"></span>
                                            </li>
                                            <li><span class="bread-blod"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-sales-area mg-tb-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-sales-chart">
                            <div class="portlet-title">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        
                                    </div>
                                </div>
                            </div>
                            <div id="morris-area-chart" style="height: 356px;">
                               
                            <div class="blog-area mg-tb-15">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                                                    <div class="hpanel blog-box responsive-mg-b-30">
                                                        <div class="panel-heading custom-blog-hd">
                                                            <div class="media clearfix">
                                                                <a class="pull-left">
                                                                    <img class="img-circle" src="<?php echo base_url() ?>asset/img/logo/1.jpg" alt="profile-picture">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="panel-body blog-pra">
                                                            <a>
                                                                <h4>NAMA CALON</h4>
                                                            </a>
                                                            <table>
                                                                <tr>
                                                                  <td>Jenis Kelamin</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                                <tr>
                                                                  <td>NIK</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                                <tr>
                                                                  <td>Tempat Lahir</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                                <tr>
                                                                  <td>Tanggal Lahir</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                                <tr>
                                                                  <td>Alamat</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                                                    <div class="hpanel blog-box responsive-mg-b-30">
                                                        <div class="panel-heading custom-blog-hd">
                                                            <div class="media clearfix">
                                                                <a class="pull-left">
                                                                    <img class="img-circle" src="<?php echo base_url() ?>asset/img/logo/2.jpg" alt="profile-picture">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="panel-body blog-pra">
                                                            <a>
                                                                <h4>NAMA CALON</h4>
                                                            </a>
                                                            <table>
                                                                <tr>
                                                                  <td>Jenis Kelamin</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                                <tr>
                                                                  <td>NIK</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                                <tr>
                                                                  <td>Tempat Lahir</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                                <tr>
                                                                  <td>Tanggal Lahir</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                                <tr>
                                                                  <td>Alamat</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                                                    <div class="hpanel blog-box">
                                                        <div class="panel-heading custom-blog-hd">
                                                            <div class="media clearfix">
                                                                <a>
                                                                    <img class="img-circle" src="<?php echo base_url() ?>asset/img/logo/3.jpg" alt="profile-picture">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="panel-body blog-pra">
                                                            <a href="blog_details.html">
                                                                <h4>NAMA CALON</h4>
                                                            </a>
                                                            <table>
                                                                <tr>
                                                                  <td>Jenis Kelamin</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                                <tr>
                                                                  <td>NIK</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                                <tr>
                                                                  <td>Tempat Lahir</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                                <tr>
                                                                  <td>Tanggal Lahir</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                                <tr>
                                                                  <td>Alamat</td>
                                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</td>
                                                                  <td></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-copy-right">
                            <p>Copyright &copy; 2018 <a href="https://colorlib.com/wp/templates/">Colorlib</a> All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


