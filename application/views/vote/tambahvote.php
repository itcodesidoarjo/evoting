   
<?php 
    function limit_words($string, $word_limit){
        $words = explode(" ",$string);
        return implode(" ",array_splice($words,0,$word_limit));
    }

   
?>
   <!-- Mobile Menu end -->
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list single-page-breadcome">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="breadcome-heading">
                                            <form role="search" class="">
                                                <input type="text" placeholder="Search..." class="form-control">
                                                <a href=""><i class="fa fa-search"></i></a>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="#">VOTE</a> <span class="bread-slash"></span>
                                            </li>
                                            <li><span class="bread-blod"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="blog-area mg-tb-15">
            <div class="container-fluid">
                <div class="row">
                 <?php foreach ($data_vote as $a) :?>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="hpanel blog-box mg-t-30 responsive-mg-b-0">
                            <div class="panel-heading custom-blog-hd">
                                <div class="media clearfix">
                                    <a class="pull-left">
                                            <img style="min-width: 76px; max-width: 76px; max-height : 76px;  min-height : 76px;" class="img-circle" src="<?php echo base_url() . 'asset/img/'.$a->foto_anggota?>" alt="profile-picture">
                                        </a>
                                    <div class="media-body blog-std">
                                        <p><?php echo $a->level_undangan?></p><p><span class="font-bold" style="font-size :15px"><?php echo $a->nama_anggota?></span> </p> 
                                        <p class="text-muted"><?php echo $a->nik_anggota?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body blog-pra">
                                <div class="blog-img">
                                    <center>
                                    <img  style="min-width: 200px; max-width: 200px; max-height : 200px;  min-height : 200px;" src="<?php echo base_url() . 'asset/img/'.$a->foto_anggota?>" alt="" /></center>
                                </div>
                                <a href="blog_details.html">
                                    <h4>Visi & MISI</h4>
                                </a>
                                <p>
                                    <?php 
                                      $visi = limit_words($a->visi_kandidat, 10);
                                      echo $visi."...";
                                    ?><a href="" style="color:red;"> Lihat Detail</a>
                                </p>
                            </div>
                            <div class="panel-footer">
                                 <button id="basicPrimaryWidth" class="btn btn-primary" type="submit"><i class="fa fa-eye"> </i> DETAIL 
                                </button>
                               
                                 <button id="basicSuccessWidth" class="btn btn-success pull-right" type="submit"><i class="fa fa-dropbox" aria-hidden="true"></i> VOTING
                                </button>
                               
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
