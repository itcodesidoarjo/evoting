<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<div class="page">
<div class="subpage">

    <table border="0" width="100%" style="text-align: left;">
        <thead>
            <tr>
                <th align="center" width="13%">
                    <img  style ="width:auto; height:auto; max-width:140px; max-height:200px; display:block;"  src="<?php echo base_url() ?>asset/img/logo/logo.png" alt="" /> </th>
                <th>  
                </th>
            </tr>
        </thead>
    </table>
     
     <hr style="margin-top:2px;">
     <h5 align="center" style="margin-top: 0px;">
        <b>Laporan Data Vote</b>
     </h5>
     <?php if (!empty($periode)): ?>
         <h5 align="center" style="margin-top:-18px">
             <b> <?= Html::encode(strtoupper($periode)) ?> </b>
         </h5>
     <?php endif; ?>
     <hr style="margin-top:0px; margin-bottom:5px; margin-top: -15px;">
<?php
    $no    = 1;
    
?>

    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="table-layout: fixed;" >
        <thead>
            <tr>
                <th align="center" style="padding: 2px 3px 2px 3px; height: 0.7cm;font-size:72%; border-bottom: 1px solid; width: 4%; ">
                    No
                </th>

                <th style="padding: 2px 3px 2px 3px; height: 0.7cm;font-size:72%; border-bottom: 1px solid; width: 10%;  ">
                    Tgl. Undangan
                </th>
                <th style="padding: 2px 3px 2px 3px; height: 0.7cm;font-size:72%; border-bottom: 1px solid; width: 10%;  ">
                    Kode Undangan
                </th>
                <th style="padding: 2px 3px 2px 3px; height: 0.7cm; font-size:72%; border-bottom: 1px solid; width: 10%; ">
                    No. Urut
                </th>
                <th  style="padding: 2px 3px 2px 3px; height: 0.7cm; font-size:72%; border-bottom: 1px solid; width: 10%; ">
                    Nama
                </th>
                 <th align="center" style="padding: 2px 3px 2px 3px; height: 0.7cm;font-size:72%; border-bottom: 1px solid; width: 10%; ">
                    Kandidat
                </th>
                <th style="padding: 2px 3px 2px 3px; height: 0.7cm;font-size:72%; border-bottom: 1px solid; width: 10%;  ">
                    Jumlah vote
                </th>
            </tr>
       
            <?php 
                $no =1;                        
                foreach ($tbl_vote as $key):
            ?>

            <tr>
                <td align="center" style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo $no ?>
                </td>
                <td style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo  date('d M Y', strtotime($key->tgl_undangan));?>
                </td>
                <td align="center" style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo 'UD0'.$key->id_undangan ?>
                </td>
                <td style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo $key->no_ballot; ?>
                </td>
                <td style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo $key->nama_anggota; ?>
                </td>
                <td align="center" style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo$key->level_ballot;  ?>
                </td>
                <?php 
                    if ($key->level_ballot == "SPG PRAM") {
                ?>
                        <td style="padding: 4px 4px 4px 4px; font-size:70%; ">
                            <?php if(empty($key->spg)){
                                 echo '0';
                            }else {
                                 echo $key->spg;
                            }
                            ?>
                        </td>
                <?php }
                ?>
                <?php 
                    if ($key->level_ballot == "CSO KASIR") {
                ?>
                        <td style="padding: 4px 4px 4px 4px; font-size:70%; ">
                            <?php if(empty($key->cso)){
                                 echo '0';
                            }else {
                                 echo $key->cso;
                            }
                            ?>
                        </td>
                <?php }
                ?>
                 <?php 
                    if ($key->level_ballot == "SPV COOR") {
                ?>
                        <td style="padding: 4px 4px 4px 4px; font-size:70%; ">
                            <?php if(empty($key->spv)){
                                 echo '0';
                            }else {
                                 echo $key->spv;
                            }
                            ?>
                        </td>
                <?php }
                ?>
               
               
            </tr>
             <?php endforeach; ?>
            
                    </table>
                </td>
            </tr>

    </table>
               </div>
    </div>
</body>
</html>

<style type="text/css">
    
 #btm td{
    border-bottom: 1px solid black;
 }
    body{
    font-family : Arial;
    font-style: bold;
    margin: 0;
    background-color: #404040;
}
.page {
    width: 210mm;
    min-height:297mm ;
    padding: 1mm;
    margin: 0mm auto;
    background: white;
}
.subpage {
    margin-left:15mm;
    margin-right:5mm;
    margin-top:5mm;
}

table td {  
    word-wrap: break-word;         
    overflow-wrap: break-word;     
}

@page {
    size: A4;
    margin: 0;

}

@media print {
    .page {
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: always;
    }
}
</style>


