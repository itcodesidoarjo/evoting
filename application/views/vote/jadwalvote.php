<div class="review-tab-pro-inner"> 
    <?php if($this->session->flashdata('pesan')): ?>
        <div class="alert alert-success alert-success-style1 alert-success-stylenone">
            <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                    <span class="icon-sc-cl" aria-hidden="true">×</span>
                </button>
            <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
            <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
        </div>
    <?php endif; ?>
     <?php if($this->session->flashdata('error')): ?>
        <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
            <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                    <span class="icon-sc-cl" aria-hidden="true">×</span>
                </button>
            <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
            <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
        </div>
    <?php endif; ?>

    <?php $vot=$vote; 

    if (!empty($vot)):?>
    <?php $kodever = $anggotastatus[0]->statusver_anggota; 
    //if ($kodever == 1):
    ?>
        <ul id="myTab3" class="tab-review-design">
        <?php if (!empty($pram)):?>
            <li class="active"><a href="#description"><i class="fa fa-users text-info" aria-hidden="true"></i> SPG PRAM</a></li>
        <?php endif; ?>
        <?php if (!empty($kasir)):?>
            <li><a href="#reviews"><i class="fa fa-users text-info" aria-hidden="true"></i> CSO KASIR</a></li>
        <?php endif; ?>
        <?php if (!empty($spv)):?>
            <li><a href="#INFORMATION"><i class="fa fa-users text-info" aria-hidden="true"></i> SPV COOR</a></li>
        <?php endif; ?>
        </ul>
       
        <div id="myTabContent" class="tab-content custom-product-edit">
        <?php if (!empty($pram)):?>
            <div class="product-tab-list tab-pane fade active in" id="description">
                <div class="row">
                <?php foreach ($pram as $a) :?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="hpanel blog-box mg-t-30 responsive-mg-b-0">
                            <?php echo form_open('vote/voting'); ?>
                            <input type="hidden" name="id_ballot" value="<?php echo $a->id_ballot?>">
                            <input type="hidden" name="id_undangan" value="<?php echo $a->id_undangan?>">
                            <input type="hidden" name="suara" value="1">
                            <div class="panel-heading custom-blog-hd">
                                <div class="media clearfix">
                                    <a class="pull-left">
                                            <img style="min-width: 76px; max-width: 76px; max-height : 76px;  min-height : 76px;" class="img-circle" src="<?php echo base_url() . 'asset/img/'.$a->foto_anggota?>" alt="profile-picture">
                                        </a>
                                    <div class="media-body blog-std">
                                        <p><?php echo $a->level_ballot?></p><p><span class="font-bold" style="font-size :15px"><?php echo $a->nama_anggota?></span> </p> 
                                        <p class="text-muted"><?php echo $a->nik_anggota?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body blog-pra">
                                <div class="blog-img">
                                    <center>
                                    <img  style="min-width: 200px; max-width: 200px; max-height : 200px;  min-height : 200px;" src="<?php echo base_url() . 'asset/img/'.$a->foto_anggota?>" alt="" /></center>
                                </div>
                                <a href="blog_details.html">
                                    <h4>Visi & MISI</h4>
                                </a>
                                <p>
                                    <?php 
                                       $visi = limit_words($a->visi_kandidat, 10);
                                       echo $visi."...";
                                    ?><a href="" style="color:red;"> Lihat Detail</a>
                                </p>
                            </div>
                            <div class="panel-footer">
                                <a  id="basicPrimaryWidth"  class="btn btn-primary" href="<?php echo base_url('kandidat/detailkandidat/'.$a->id_kandidat);?>"> <i class="fa fa-eye"> </i>DETAIL</a>
                                <?php if (empty($hitungpram)):?>
                                 <button id="basicSuccessWidth" class="btn btn-success pull-right" type="submit"><i class="fa fa-dropbox" aria-hidden="true"></i> VOTING
                                </button>
                                <?php endif; ?>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($kasir)):?>
            <div class="product-tab-list tab-pane fade" id="reviews">
                <div class="row">
                <?php foreach ($kasir as $a) :?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="hpanel blog-box mg-t-30 responsive-mg-b-0">
                            <?php echo form_open('vote/voting'); ?>
                            <input type="hidden" name="id_ballot" value="<?php echo $a->id_ballot?>">
                            <input type="hidden" name="id_undangan" value="<?php echo $a->id_undangan?>">
                            <input type="hidden" name="suara1" value="1">
                            <div class="panel-heading custom-blog-hd">
                                <div class="media clearfix">
                                    <a class="pull-left">
                                            <img style="min-width: 76px; max-width: 76px; max-height : 76px;  min-height : 76px;" class="img-circle" src="<?php echo base_url() . 'asset/img/'.$a->foto_anggota?>" alt="profile-picture">
                                        </a>
                                    <div class="media-body blog-std">
                                        <p><?php echo $a->level_ballot?></p><p><span class="font-bold" style="font-size :15px"><?php echo $a->nama_anggota?></span> </p> 
                                        <p class="text-muted"><?php echo $a->nik_anggota?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body blog-pra">
                                <div class="blog-img">
                                    <center>
                                    <img  style="min-width: 200px; max-width: 200px; max-height : 200px;  min-height : 200px;" src="<?php echo base_url() . 'asset/img/'.$a->foto_anggota?>" alt="" /></center>
                                </div>
                                <a href="blog_details.html">
                                    <h4>Visi & MISI</h4>
                                </a>
                                <p>
                                    <?php 
                                       $visi = limit_words($a->visi_kandidat, 10);
                                       echo $visi."...";
                                    ?><a href="" style="color:red;"> Lihat Detail</a>
                                </p>
                            </div>
                            <div class="panel-footer">
                                <a  id="basicPrimaryWidth"  class="btn btn-primary" href="<?php echo base_url('kandidat/detailkandidat/'.$a->id_kandidat);?>"> <i class="fa fa-eye"> </i>DETAIL</a>
                                <?php if (empty($hitungkasir)):?>
                                 <button id="basicSuccessWidth" class="btn btn-success pull-right" type="submit"><i class="fa fa-dropbox" aria-hidden="true"></i> VOTING
                                </button>
                                <?php endif; ?>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($spv)):?>
            <div class="product-tab-list tab-pane fade" id="INFORMATION">
                <div class="row">
                <?php foreach ($spv as $a) :?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="hpanel blog-box mg-t-30 responsive-mg-b-0">
                            <?php echo form_open('vote/voting'); ?>
                            <input type="hidden" name="id_ballot" value="<?php echo $a->id_ballot?>">
                            <input type="hidden" name="id_undangan" value="<?php echo $a->id_undangan?>">
                            <input type="hidden" name="suara2" value="1">

                            <div class="panel-heading custom-blog-hd">
                                <div class="media clearfix">
                                    <a class="pull-left">
                                            <img style="min-width: 76px; max-width: 76px; max-height : 76px;  min-height : 76px;" class="img-circle" src="<?php echo base_url() . 'asset/img/'.$a->foto_anggota?>" alt="profile-picture">
                                        </a>
                                    <div class="media-body blog-std">
                                        <p><?php echo $a->level_ballot?></p><p><span class="font-bold" style="font-size :15px"><?php echo $a->nama_anggota?></span> </p> 
                                        <p class="text-muted"><?php echo $a->nik_anggota?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body blog-pra">
                                <div class="blog-img">
                                    <center>
                                    <img  style="min-width: 200px; max-width: 200px; max-height : 200px;  min-height : 200px;" src="<?php echo base_url() . 'asset/img/'.$a->foto_anggota?>" alt="" /></center>
                                </div>
                                <a href="blog_details.html">
                                    <h4>Visi & MISI</h4>
                                </a>
                                <p>
                                    <?php 
                                       $visi = limit_words($a->visi_kandidat, 10);
                                       echo $visi."...";
                                    ?><a href="" style="color:red;"> Lihat Detail</a>
                                </p>
                            </div>
                            <div class="panel-footer">
                                 <a  id="basicPrimaryWidth"  class="btn btn-primary" href="<?php echo base_url('kandidat/detailkandidat/'.$a->id_kandidat);?>"> <i class="fa fa-eye"> </i>DETAIL</a>
                                 <?php if (empty($hitungspv)):?>
                                 <button id="basicSuccessWidth" class="btn btn-success pull-right" type="submit"><i class="fa fa-dropbox" aria-hidden="true"></i> VOTING
                                </button>
                                <?php endif; ?>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
        </div>
    <?php endif; ?>
  <!--   <?php if ($kodever == 0):?>
    <div class="alert alert-warning alert-success-style3 alert-st-bg2">
        <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                <span class="icon-sc-cl" aria-hidden="true">×</span>
            </button>
        <i class="fa fa-exclamation-triangle adminpro-warning-danger admin-check-pro admin-check-pro-clr2" aria-hidden="true"></i>
        <p><strong>Warning!</strong> Silahkan masukkan verifikasi kode terlebih dahulu.</p>
    </div>
     <br>
    <br><br><br><br><br><br><br>
    <div class="row">
        <?php echo form_open('vote/inputverifikasi'); ?>
        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
            <label class="login2 pull-right pull-right-pro">Masukkan Kode</label>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="input-group custom-go-button">
                <input type="text" name="kode" class="form-control">
                <span class="input-group-btn"><button type="submit" class="btn btn-primary">Konfirmasi</button></span>
            </div>
        </div>
        <?php echo form_close(); ?>
         <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
        </div>
    </div>
    <br>
    <br><br><br><br><br><br><br>
    <?php endif; ?> -->
    <?php// endif; ?>
    <?php if (empty($vot)):?>
        <div class="alert alert-warning alert-success-style3 alert-st-bg2">
        <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                <span class="icon-sc-cl" aria-hidden="true">×</span>
            </button>
        <i class="fa fa-exclamation-triangle adminpro-warning-danger admin-check-pro admin-check-pro-clr2" aria-hidden="true"></i>
        <p>Tidak ada data voting hari ini.</p>
    </div>
    <?php endif; ?>

</div>



<?php 
    function limit_words($string, $word_limit){
        $words = explode(" ",$string);
        return implode(" ",array_splice($words,0,$word_limit));
    }
?>