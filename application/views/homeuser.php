<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E-Voting</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>asset/anggota/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>asset/anggota/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>asset/anggota/vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>asset/anggota/css/stylish-portfolio.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>asset/css/modals.css">
  </head>

  <body id="page-top">

    <!-- Navigation -->
    <a class="menu-toggle rounded" href="#">
      <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li class="sidebar-brand">
          <a class="js-scroll-trigger" href="#page-top">MENU</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#page-top">Home</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="<?php echo base_url()?>login">Login Admin</a>
        </li>
   
      </ul>
    </nav>

    <!-- Header -->
    <header class="masthead d-flex">
      <div class="container text-center my-auto">
        <h1 class="mb-1" style="color:white;">Selamat Datang</h1>
        <h3 class="mb-5">
          <em></em><h5 style="color: white; text-align:left;"><?php echo validation_errors(); ?></h5>
        </h3>
        <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">LOGIN</a>
      </div>
      <div class="overlay"></div>
    </header>

    <!-- Call to Action -->
    <section class="content-section bg-primary text-white" id="about">
      <div class="container text-center">
        <?php if($this->session->flashdata('error')): ?>
          <h5 style="color:red; "><?=$this->session->flashdata('error')?></h5>
        <?php endif; ?>
        <h2 class="mb-4">LOGIN</h2>
       <?php echo form_open('login/authanggota'); ?>
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
              <div class="form-group">
                  <input class="form-control" name="username" type="text" required="" placeholder="Username">
              </div>
            </div>
            <div class="col-md-6"></div>
          </div>
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="form-group">
                    <input class="form-control" type="password" name="password" required="" placeholder="Password">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <input type="text"  class="form-control" placeholder="Captcha"  name="secutity_code">
                    </div>
                     <div class="col-md-6">
                        <p ><?=$image;?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
          </div>
        <button class="btn btn-sm btn-light mr-4">Login</button>
        <a class="btn btn-sm btn-dark"href="#page-top">Cencel</a>
        <?php echo form_close(); ?>
      </div>
    </section>


    <!-- Footer -->
    <footer class="footer text-center">
      <div class="container">
        <ul class="list-inline mb-5">
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="www.facebook.com">
              <i class="icon-social-facebook"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="www.twitter.com">
              <i class="icon-social-twitter"></i>
            </a>
          </li>
        </ul>
        <p class="text-muted small mb-0">Copyright &copy; E-Voting v.1.0002 2018</p>
      </div>
    </footer>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url();?>asset/anggota/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>asset/anggota/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url();?>asset/anggota/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo base_url();?>asset/anggota/js/stylish-portfolio.min.js"></script>

  </body>

</html>
