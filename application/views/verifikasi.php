<div class="review-tab-pro-inner"> 
<?php if($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success alert-success-style1 alert-success-stylenone">
        <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                <span class="icon-sc-cl" aria-hidden="true">×</span>
            </button>
        <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
        <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
    </div>
<?php endif; ?>
 <?php if($this->session->flashdata('error')): ?>
    <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
        <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                <span class="icon-sc-cl" aria-hidden="true">×</span>
            </button>
        <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
        <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
    </div>
<?php endif; ?>
<div class="alert alert-warning alert-success-style3 alert-st-bg2">
    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
            <span class="icon-sc-cl" aria-hidden="true">×</span>
        </button>
    <i class="fa fa-exclamation-triangle adminpro-warning-danger admin-check-pro admin-check-pro-clr2" aria-hidden="true"></i>
    <p><strong>Warning!</strong> Silahkan masukkan verifikasi kode login terlebih dahulu.</p>
</div>
 <br>
<br><br><br><br><br><br><br>
<div class="row">
    <?php echo form_open('vote/logver'); ?>
    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
        <label class="login2 pull-right pull-right-pro">Masukkan Kode</label>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
        <div class="input-group custom-go-button">
            <input type="text" name="kode" class="form-control">
            <span class="input-group-btn"><button type="submit" class="btn btn-primary">Konfirmasi</button></span>
        </div>
    </div>
    <?php echo form_close(); ?>
     <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
    </div>
</div>
<br>
<br><br><br><br><br><br><br>
</div>