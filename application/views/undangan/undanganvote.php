<!-- Mobile Menu end -->
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list single-page-breadcome">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="breadcome-heading">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <ul class="breadcome-menu">
                                    <li><a href="#">Undangan Vote</a> <span class="bread-slash"></span>
                                    </li>
                                    <li><span class="bread-blod"></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="data-table-area mg-tb-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php if($this->session->flashdata('pesan')): ?>
                     <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                            <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                    <span class="icon-sc-cl" aria-hidden="true">×</span>
                                </button>
                            <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                            <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
                        </div>
                    <?php endif; ?>
                     <?php if($this->session->flashdata('error')): ?>
                        <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
                            <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                    <span class="icon-sc-cl" aria-hidden="true">×</span>
                                </button>
                            <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                            <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
                        </div>
                    <?php endif; ?>
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
<div class="sparkline13-list">
    <div class="sparkline13-hd">
        <div class="main-sparkline13-hd">
            <h1>Data <span class="table-project-n">Undangan Vote</span></h1>
        </div>
    </div>
    <div class="sparkline13-graph">
        <div class="datatable-dashv1-list custom-datatable-overright">
            <a href="<?php echo base_url();?>undanganvote/tambahundangan" type="submit" class="btn btn-success"><i class="fa fa-plus"></i>  Tambah</a>
            <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="false" data-show-toggle="false" data-resizable="false" data-cookie="false"
                data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="false" data-toolbar="#toolbar">
                <thead>
                    <tr>
                        <th data-field="id">No</th>
                        <th>Tgl. Undangan</th>
                        <th>Periode</th>
                        <th>Nama</th>
                        <th>Jenis Kandidat</th>
                        <th>No Urut</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $no=1;
                    foreach($vote as $key) : ?>
                    <tr>
                        <td><?=$no++?></td>
                        <td><?=date('d M Y', strtotime($key->tgl_undangan));?></td>
                        <td><?=$key->periode_kandidat?></td>
                        <td><?=$key->nama_anggota?></td>
                        <td><?=$key->level_kandidat?></td>
                        <td><?=$key->no_ballot?></td>
                        <td>
                            <a href="<?php echo base_url('undanganvote/detailundangan/'.$key->id_ballot);?>"> <i class="glyphicon glyphicon-eye-open"></i></a> 
                            <?php if($this->session->userdata('logged_in')) : ?>
                            <a href="<?php echo base_url('undanganvote/hapusundangan/'.$key->id_ballot);?>" onclick="return confirm('Anda yakin menghapus data pemilik?')"><i class="glyphicon glyphicon-trash"></i></a>
                            <a href="<?php echo base_url('undanganvote/editundangan/'.$key->id_ballot);?>"><i class="glyphicon glyphicon-pencil"></i></a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
