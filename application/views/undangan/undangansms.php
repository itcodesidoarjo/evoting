<!-- Mobile Menu end -->
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list single-page-breadcome">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <ul class="breadcome-menu">
                                    <li><a href="#">Kirim Pesan SMS</a> <span class="bread-slash"></span>
                                    </li>
                                    <li><span class="bread-blod"></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="data-table-area mg-tb-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
<div class="sparkline13-list">
    <div class="sparkline13-hd">
        <?php if($this->session->flashdata('pesan')): ?>
             <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                            <span class="icon-sc-cl" aria-hidden="true">×</span>
                        </button>
                    <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                    <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
                </div>
            <?php endif; ?>
             <?php if($this->session->flashdata('error')): ?>
                <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                            <span class="icon-sc-cl" aria-hidden="true">×</span>
                        </button>
                    <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                    <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
                </div>
            <?php endif; ?>
          <div class="panel-heading hbuilt">
                                <div class="p-xs">
                <form action="<?php echo base_url('undangansms/sendmsg') ?>" method="post" class="form-horizontal">

                            <div class="form-group">
                                <label class="col-sm-1 control-label text-left">To:</label>
                                <div class="col-sm-4">
                                    <input type="text" name="mobile" class="form-control input-sm" placeholder="Masukkan No Hp">
                                </div>
                                <div class="col-md-12">
                        <?php echo form_error('mobile', '<span class="text-danger">','</span>') ?>
                                    </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-1 control-label text-left">Text:</label>
                                <div class="col-sm-11">
                                    <textarea name="message" cols="40" rows="5" placeholder=""></textarea>
                                </div>
                                    <div class="col-md-12">
                        <?php echo form_error('message', '<span class="text-danger">','</span>') ?>
                                    </div>
                            </div>
                        <input type="checkbox" name="check_list[]" value="PHP"><label>Kirim ke semua</label>
                    </div>
                </div>
                    <button class="btn btn-primary ft-compse">Kirim Pesan</button>
              
                </form>
    </div>
    <div class="sparkline13-graph">
    </div>
</div>


                           