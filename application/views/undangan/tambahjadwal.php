      <!-- Mobile Menu end -->
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list single-page-breadcome">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="breadcome-heading">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="#">Tambah</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Jadwal Undangan</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <div class="mailbox-compose-area mg-tb-15">
            <?php if($this->session->flashdata('pesan')): ?>
            <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                    </button>
                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
            </div>
        <?php endif; ?>
         <?php if($this->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                    </button>
                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
            </div>
        <?php endif; ?>
        
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="hpanel email-compose mg-b-15">
                            <div class="panel-heading hbuilt">
                                <div class="p-xs h4">
                                    Jadwal Undangan Baru
                                </div>
                            </div>
                            <?php echo form_open('undanganvote/inputjadwal'); ?>
                            <div class="panel-heading hbuilt">
                                <div class="p-xs">
                                    <div class="row">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <input name="tentang_undangan" type="text" class="form-control input-sm" placeholder="Tentang">
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                             <div class="form-group" id="data_3">
                                                 <div class="col-sm-12">
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input name="tgl_undangan" type="text" class="form-control" placeholder="Tanggal" >
                                                    </div>
                                                </div>
                                            </div>
                                            
                                    </div>
                                    <div class="row">
                                        <div   class="panel-body no-padding">
                                            <textarea  name="keteranagan_undangan" class="summernote6"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-default pull-right"><i class="fa fa-edit"></i> Save</button> 
                                <a href="<?php echo base_url() ?>undanganvote/tambahundangan" class="btn btn-default "><i class="fa fa-trash"></i> Discard</a>
                            </div>
                            <?php echo form_close(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>