<div class="review-tab-pro-inner">
    <ul id="myTab3" class="tab-review-design">
        <?php if($this->session->flashdata('pesan')): ?>
             <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                            <span class="icon-sc-cl" aria-hidden="true">×</span>
                        </button>
                    <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                    <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
                </div>
            <?php endif; ?>
             <?php if($this->session->flashdata('error')): ?>
                <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                            <span class="icon-sc-cl" aria-hidden="true">×</span>
                        </button>
                    <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                    <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
                </div>
            <?php endif; ?>
    <li class="active"><a href="#description"><i class="fa fa-pencil" aria-hidden="true"></i>Tambah Data Undangan</a></li>
    </ul>
        <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
                <div class="row">
                    
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <!-- <a href="<?php echo base_url();?>undanganvote/tambahjadwal" type="submit" class="btn btn-success"><i class="fa fa-plus"></i>  Tambah Jadwal</a> -->
                    <?php echo form_open('undanganvote/inputballot'); ?>
                        <div class="review-content-section">
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <input type="text" disabled="" class="form-control" value="<?php echo $this->session->userdata('ses_username'); ?>" placeholder="Nama Admin">
                                 <input type="hidden" class="form-control" required="" name="id_admin" value="<?php echo $this->session->userdata('ses_id_admin'); ?>" placeholder="Nama Admin">
                            </div>
                           <!--   <div class="chosen-select-single mg-b-20">
                                <select id="jan" name="id_undangan" data-placeholder="Choose a Country..." class="chosen-select" tabindex="-1">
                                        <option value="">Pilih Jadwal Undangan</option>
                                         <?php  foreach ($tbl_undangan as $key): ?>
                                             <option value="<?=$key->id_undangan?>"><?= date('d M Y', strtotime($key->tgl_undangan))." / ".$key->tentang_undangan?></option>
                                        <?php endforeach; ?>
                                </select>
                            </div> -->
                             <div class="form-group" id="data_3">
                                <div class="chosen-select-single mg-b-20">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input name="tgl_undangan" type="text" class="form-control" placeholder="Tanggal" >
                                    </div>
                                </div>
                            </div>
                        <!--      <div class="chosen-select-single mg-b-20">
                                <div class="input-group custom-go-button">
                                    <input type="text" id="nama_kandidat" placeholder="Cari Kandidat" readonly required class="form-control">
                                    <span class="input-group-btn"><a class="btn btn-primary"href="#" data-toggle="modal" data-target="#cari"><i class="fa fa-search" aria-hidden="true"></i></a></span>
                                </div>
                            </div> -->
                            <!--  <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                <input type="text" name="level_ballot" id="level_ballot" readonly class="form-control" placeholder="Jenis Kandidat">
                            </div> -->
                            <input type="hidden" name="id_kandidat" id="id_kandidat">
                             <div class="chosen-select-single mg-b-20">
                                <select id="ballot" name="level_ballot" required data-placeholder="Choose a Country..." class="chosen-select" tabindex="-1">
                                    <option > Pilih Jenis Kandidat</option>
                                    <option value="SPG PRAM">SPG PRAM</option>
                                    <option value="CSO KASIR">CSO KASIR</option>
                                    <option value="SPV COOR">SPV COOR</option>
                                </select>
                            </div>
                          <!--    <div class="chosen-select-single mg-b-20">
                                <select  name="id_kandidat" data-placeholder="Choose a Country..." class="chosen-select" tabindex="-1">
                                        <option value="">Pilih Calon Kandidat</option>
                                        <?php  foreach ($tbl_kandidat as $key): ?>
                                             <option  value="<?=$key->id_kandidat?>"><?= $key->nik_anggota." / ".$key->nama_anggota?> 
                                        <?php endforeach; ?>
                                </select>
                            </div> -->
                           <!--  <input class="touchspin3 form-control" type="text" placeholder="No. Urut" id="no_ballot" name="no_ballot" style="display: block;"> -->
                            <br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="text-center mg-b-pro-edt custom-pro-edt-ds">
                            <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Simpan </button>
                            <a href="<?php echo base_url() ?>undanganvote" type="button" class="btn btn-warning waves-effect waves-light">Batal
                                </a>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
</div>

<div class="modal fade" id="cari" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Cari Kandidat</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="width:96%;margin:auto;">
                    <table class="table table-bordered table-hover" id="manageMemberTable">
                         <thead>
                            <tr>
                                <th data-field="id">No</th>
                                <th>No. Identitas</th>
                                <th>Nama</th>
                                <th>Jenis Kandidat</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no=1;
                            foreach($tbl_kandidat as $key) : ?>
                            <tr onclick="pilih(<?php echo $key->id_kandidat; ?>)" style="cursor:pointer;">
                                <td><?=$no++?></td>
                                <td><?=$key->nik_anggota?></td>
                                <td><?=$key->nama_anggota?></td>
                                <td><?=$key->level_kandidat?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script   src="https://code.jquery.com/jquery-3.3.1.js"   integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="   crossorigin="anonymous"></script>
  <script type="text/javascript">
        function pilih(id){
            <?php 
                 $query=$this->db->query('SELECT a.*, b.*
                    FROM tbl_kandidat a
                    LEFT JOIN tbl_anggota b ON b.id_anggota = a.id_anggota');
                foreach($query->result() as $s){  
            ?>
            if (id == <?php echo $s->id_kandidat; ?>) {
                document.getElementById("level_ballot").value="<?php echo $s->level_kandidat;?>";
                document.getElementById("nama_kandidat").value="<?php echo $s->nama_anggota;?>";
                document.getElementById("id_kandidat").value="<?php echo $s->id_kandidat;?>";
                $("#cari").modal('hide');
            }
            <?php } ?>
        }
  </script>
