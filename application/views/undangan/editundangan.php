<div class="review-tab-pro-inner">
    <ul id="myTab3" class="tab-review-design">
    <li class="active"><a href="#description"><i class="fa fa-pencil" aria-hidden="true"></i>Edit Data Undangan</a></li>
    </ul>
        <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
                <div class="row">
                    
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <!-- <a href="<?php echo base_url();?>undanganvote/tambahjadwal" type="submit" class="btn btn-success"><i class="fa fa-plus"></i>  Tambah Jadwal</a> -->
                    <?php echo form_open('undanganvote/updateundangan'); ?>
                        <div class="review-content-section">
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <input type="text" disabled="" class="form-control" value="<?php echo $this->session->userdata('ses_username'); ?>" placeholder="Nama Admin">
                                 <input type="hidden" class="form-control" required="" name="id_admin" value="<?php echo $this->session->userdata('ses_id_admin'); ?>" placeholder="Nama Admin">
                                   <?php  foreach ($vote as $key): ?>
                                 <input type="hidden" class="form-control"  name="id_ballot" value="<?=$key->id_ballot?>" placeholder="Nama Admin">
                                 <input type="hidden" class="form-control"  name="level_kandidat" value="<?=$key->level_kandidat?>" placeholder="Nama Admin">
                                 <input type="hidden" class="form-control"  name="tgl" value="<?=$key->tgl?>" placeholder="Nama Admin">
                            <?php endforeach; ?>

                            </div>
                             <!-- <div class="chosen-select-single mg-b-20">
                                <select name="id_undangan" data-placeholder="Choose a Country..." class="chosen-select" tabindex="-1">
                                        <option value="">Pilih Jadwal Undangan</option>
                                         <?php  foreach ($tbl_undangan as $key): ?>
                                             <option value="<?=$key->id_undangan?>"><?= $key->tgl_undangan." / ".$key->tentang_undangan?></option>
                                        <?php endforeach; ?>
                                </select>
                            </div> -->
                            <!--  <div class="chosen-select-single mg-b-20">
                                <select  name="id_kandidat" data-placeholder="Choose a Country..." class="chosen-select" tabindex="-1">
                                        <option value="">Pilih Calon Kandidat</option>
                                        <?php  foreach ($tbl_kandidat as $key): ?>
                                             <option  value="<?=$key->id_kandidat?>"><?= $key->no_kandidat." / ".$key->nama_kandidat." / ".$key->level_kandidat?> 
                                        <?php endforeach; ?>
                                </select>
                            </div> -->
                          <?php  foreach ($vote as $key): ?>
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                <input type="text" name="world_anggota" readonly="" value="<?= date('d M Y', strtotime($key->tgl_undangan));?>" class="form-control" placeholder="World">
                            </div>

                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                <input type="text" name="world_anggota" readonly="" value="<?=$key->level_kandidat?>" class="form-control" placeholder="World">
                            </div>
                            
                            <input class="touchspin3 form-control" type="text" value="<?=$key->no_ballot?>" placeholder="No. Urut"  name="no_ballot" style="display: block;">
                            <?php endforeach; ?>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="text-center mg-b-pro-edt custom-pro-edt-ds">
                            <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Simpan </button>
                            <a href="<?php echo base_url() ?>undanganvote" type="button" class="btn btn-warning waves-effect waves-light">Batal
                                </a>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
</div>
