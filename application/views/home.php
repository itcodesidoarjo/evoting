    <?php if (!empty($vote)):?>
            <!-- Mobile Menu end -->
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list">
                                <?php if($this->session->flashdata('pesan')): ?>
                                    <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                                        <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                                <span class="icon-sc-cl" aria-hidden="true">×</span>
                                            </button>
                                        <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                                        <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
                                    </div>
                                <?php endif; ?>
                                 <?php if($this->session->flashdata('error')): ?>
                                    <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
                                        <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                                <span class="icon-sc-cl" aria-hidden="true">×</span>
                                            </button>
                                        <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                                        <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
                                    </div>
                                <?php endif; ?>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="breadcome-heading">

                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Dashboard</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-admin container-fluid">
            <div class="row admin text-center">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="admin-content analysis-progrebar-ctn res-mg-t-15">
                                <?php foreach ($totalvote as $key ):?>
                                        <?php foreach ($totalanggota as $key1 ):?>
                                <h4 class="text-left text-uppercase"><b>Data Vote</b></h4>
                                <div class="row vertical-center-box vertical-center-box-tablet">
                                    <div class="col-xs-3 mar-bot-15 text-left">
                                        <label class="label bg-green"><?php echo $key->totalhitung/$key1->totalanggota*100; ?> %<i class="fa fa-level-up" aria-hidden="true"></i></label>
                                    </div>
                                    <div class="col-xs-9 cus-gh-hd-pro">
                                        <h2 class="text-right no-margin"><?php echo $key->totalhitung; ?> </h2>
                                    </div>
                                </div>
                                <div class="progress progress-mini">
                                    <div style="width: <?php echo $key->totalhitung/$key1->totalanggota*100;?>%;" class="progress-bar bg-green"></div>
                                </div>
                            <?php endforeach; ?>
                            <?php endforeach; ?>
                            </div>
                        </div>
                            <?php foreach ($totalkekurangan as $key1 ):?>
                                <?php foreach ($totalanggota as $key2 ):?>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="admin-content analysis-progrebar-ctn res-mg-t-30">
                                <h4 class="text-left text-uppercase"><b>Data Belum Vote</b></h4>
                                <div class="row vertical-center-box vertical-center-box-tablet">
                                    <div class="text-left col-xs-3 mar-bot-15">
                                        <label class="label bg-blue"><?php echo $key1->tot/$key2->totalanggota*100; ?>% <i class="fa fa-level-up" aria-hidden="true"></i></label>
                                    </div>
                                    <div class="col-xs-9 cus-gh-hd-pro">
                                        <h2 class="text-right no-margin"><?php echo $key1->tot;?></h2>
                                    </div>
                                </div>
                                <div class="progress progress-mini">
                                    <div style="width: <?php echo $key1->tot/$key2->totalanggota*100; ?>%;" class="progress-bar bg-blue"></div>
                                </div>
                            </div>
                        </div>
                         <?php endforeach; ?>
                         <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div class="charts-area mg-tb-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="charts-single-pro responsive-mg-b-30">
                                <div class="alert-title">
                                    <h2>Data Voting</h2>
                                    <h6 style="color:#ff8c00;">SPV COOR</h6>
                                    <h6 style="color:#00FA9A;">SPG PRAM</h6>
                                    <h6 style="color:#00CED1;">CSO KASIR</h6>
                                <center>
                                 <table border="0">
                                    <tr valign="bottom" >
                                        <?php if (!empty($datagrafik)) :?>
                                        <?php foreach ($datagrafik as $key ):?>

                                        <?php foreach ($totalanggota as $key1 ):?>
                                        <td style="width:1.5cm; height:100mm; background-color: white;">
                                            <table >
                                                <tr>
                                                    <?php 
                                                    if ($key->level_ballot == "SPV COOR") {
                                                       $color = "#ff8c00";
                                                       $jml = $key->jmlspv;
                                                    }else if ($key->level_ballot == "SPG PRAM") {
                                                       $color = "#00FA9A";
                                                       $jml = $key->jmlpram;
                                                    }else{
                                                       $color = "#00CED1";
                                                       $jml = $key->jmlkasir;
                                                    }

                                                    $jmlpram = $jml/$key1->totalanggota*100;?>
                                                    <td align="center" style="width:9.7mm; height:<?=$jmlpram?>mm; background-color: <?=$color;?>;">
                                                       
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <?php endforeach; ?>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr valign="top">
                                         <?php if (!empty($datagrafik)) :?>
                                        <?php foreach ($datagrafik as $key ):?>
                                        <?php foreach ($totalanggota as $key1 ):?>
                                        <td style="width:10mm;" align="center">
                                             <?php 
                                                    if ($key->level_ballot == "SPV COOR") {
                                                       $color = "#ff8c00";
                                                       $jml = $key->jmlspv;
                                                    }else if ($key->level_ballot == "SPG PRAM") {
                                                       $color = "#00FA9A";
                                                       $jml = $key->jmlpram;
                                                    }else{
                                                       $color = "#00CED1";
                                                       $jml = $key->jmlkasir;
                                                    }
                                                ?>
                                            <?php $jmlpram = $jml/$key1->totalanggota*100;?>
                                             <?=$jmlpram."%"?> <br> <?php echo "NO.".$key->no_ballot;?>
                                        </td>
                                        <?php endforeach; ?>
                                         <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tr>
                                </table>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if (empty($vote)):?>
            <div class="alert alert-warning alert-success-style3 alert-st-bg2">
                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                    </button>
                <i class="fa fa-exclamation-triangle adminpro-warning-danger admin-check-pro admin-check-pro-clr2" aria-hidden="true"></i>
                <p>Tidak ada data voting hari ini.</p>
                <br><br><br>
            </div>

    <?php endif; ?>