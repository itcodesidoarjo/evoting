<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dashboard  | E-Voting</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() ?>asset/img/favicon.ico">
    <!-- Google Fonts
        ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Play:400,700" rel="stylesheet">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/bootstrap.min.css">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/font-awesome.min.css">
    <!-- owl.carousel CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/owl.transitions.css">
    <!-- animate CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/animate.css">
    <!-- normalize CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/normalize.css">
    <!-- meanmenu icon CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/meanmenu.min.css">
    <!-- main CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/main.css">
    <!-- morrisjs CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/style.css">
    <!-- responsive CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/responsive.css">
    <!-- modernizr JS
        ============================================ -->
    <script src="<?php echo base_url() ?>asset/js/vendor/modernizr-2.8.3.min.js"></script>
      <!-- summernote CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/summernote/summernote.css">

      <!-- touchspin CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/touchspin/jquery.bootstrap-touchspin.min.css">
    <!-- datapicker CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/datapicker/datepicker3.css">
    <!-- forms CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/form/themesaller-forms.css">
    <!-- colorpicker CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/colorpicker/colorpicker.css">
    <!-- select2 CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/select2/select2.min.css">
    <!-- chosen CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/chosen/bootstrap-chosen.css">
    <!-- ionRangeSlider CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/ionRangeSlider/ion.rangeSlider.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/ionRangeSlider/ion.rangeSlider.skinFlat.css">
     <!-- notifications CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/notifications/Lobibox.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/notifications/notifications.css">
    <!-- style CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/alerts.css">
    <!-- modals CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/modals.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/hhh/datatables/css/dataTables.bootstrap.min.css" />
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset/hhh/custom/css/style.css" type="text/css">
</head>

<body>

    <div class="left-sidebar-pro">
        <nav id="sidebar" class="">
            <div class="sidebar-header">
                <a href="index.html"><img class="main-logo" src="<?php echo base_url() ?>asset/img/logo/logo.png" alt="" /></a>
                <strong><img src="<?php echo base_url() ?>asset/img/logo/logosn.png" alt="" /></strong>
            </div>
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <ul class="metismenu" id="menu1">
                        <?php if ($this->session->userdata('ses_ver')==0):?>
                             <?php if($this->session->userdata('logged_anggota')) : ?>
                            <li><a title="Landing Page" href="<?php echo base_url();?>Welcome" aria-expanded="false">
                                <i class="fa big-icon fa-home icon-wrap"></i> <span class="mini-click-non">Dashboard</span></a>
                            </li>
                             <?php endif; ?>
                             <?php if($this->session->userdata('logged_in')) : ?>
                            <li><a title="Landing Page" href="<?php echo base_url();?>Welcome/verifikasi" aria-expanded="false">
                                <i class="fa big-icon fa-table icon-wrap"></i> <span class="mini-click-non">Data Anggota</span></a>
                            </li>
                            <li><a title="Landing Page" href="<?php echo base_url();?>Welcome/verifikasi" aria-expanded="false">
                                <i class="fa big-icon fa-table icon-wrap"></i> <span class="mini-click-non">Data Kandidat</span></a></li>
                            <li><a title="Landing Page" href="<?php echo base_url();?>Welcome/verifikasi" aria-expanded="false">
                                <i class="fa big-icon fa-table icon-wrap"></i> <span class="mini-click-non">Tambah Vote</span></a></li>
                             <li><a title="Landing Page" href="<?php echo base_url();?>Welcome/verifikasi" aria-expanded="false">
                                <i class=" fa big-icon fa-envelope icon-wrap"></i> <span class="mini-click-non">Kirim SMS</span></a></li>    
                            <li>
                            <li>
                                <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="fa big-icon fa-desktop icon-wrap"></i> <span class="mini-click-non">Report</span></a>
                                <ul class="submenu-angle" aria-expanded="false">
                                    <li><a title="Google Map" href="<?php echo base_url() ?>Welcome/verifikasi"><i class="fa big-icon fa-files-o icon-wrap"></i><span class="mini-sub-pro">Data Anggota</span></a></li>
                                    <li><a title="Google Map" href="<?php echo base_url() ?>Welcome/verifikasi"><i class="fa big-icon fa-files-o icon-wrap"></i><span class="mini-sub-pro">Data Kandidat</span></a></li>
                                    <li><a title="Google Map" href="<?php echo base_url() ?>Welcome/verifikasi"><i class="fa big-icon fa-files-o icon-wrap"></i><span class="mini-sub-pro">Data Vote</span></a></li>
                                    <!-- <li><a title="Data Maps" href="<?php echo base_url() ?>vote/cetak"><i class="fa big-icon fa-files-o icon-wrap"></i><span class="mini-sub-pro">Data Vote</span></a></li> -->
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if($this->session->userdata('logged_anggota')) : ?>
                            <li><a title="Landing Page" href="<?php echo base_url()?>vote" aria-expanded="false">
                                <i class="fa fa-hourglass sub-icon-mg" aria-hidden="true"></i><span class="mini-click-non">Vote</span></a>
                            </li>
                            <li><a title="Landing Page" href="<?php echo base_url() ?>vote/cetak" aria-expanded="false">
                                <i class="fa big-icon fa-desktop icon-wrap"></i> <span class="mini-click-non">Report</span></a>
                            </li>
                             <?php endif; ?>
                        <?php endif; ?>
                        <?php if ($this->session->userdata('ses_ver')==1):?>
                            <li><a title="Landing Page" href="<?php echo base_url();?>" aria-expanded="false">
                                <i class="fa big-icon fa-home icon-wrap"></i> <span class="mini-click-non">Dashboard</span></a>
                            </li>
                             <?php if($this->session->userdata('logged_in')) : ?>
                            <li><a title="Landing Page" href="<?php echo base_url();?>anggota" aria-expanded="false">
                                <i class="fa big-icon fa-table icon-wrap"></i> <span class="mini-click-non">Data Anggota</span></a>
                            </li>
                            <li><a title="Landing Page" href="<?php echo base_url();?>kandidat" aria-expanded="false">
                                <i class="fa big-icon fa-table icon-wrap"></i> <span class="mini-click-non">Data Kandidat</span></a></li>
                            <li><a title="Landing Page" href="<?php echo base_url();?>undanganvote" aria-expanded="false">
                                <i class="fa big-icon fa-table icon-wrap"></i> <span class="mini-click-non">Tambah Vote</span></a></li>
                             <li><a title="Landing Page" href="<?php echo base_url();?>undangansms" aria-expanded="false">
                                <i class=" fa big-icon fa-envelope icon-wrap"></i> <span class="mini-click-non">Kirim SMS</span></a></li>    
                            <li>
                            <li>
                                <a class="has-arrow" href="mailbox.html" aria-expanded="false"><i class="fa big-icon fa-desktop icon-wrap"></i> <span class="mini-click-non">Report</span></a>
                                <ul class="submenu-angle" aria-expanded="false">
                                    <li><a title="Google Map" href="<?php echo base_url() ?>anggota/cetak"><i class="fa big-icon fa-files-o icon-wrap"></i><span class="mini-sub-pro">Data Anggota</span></a></li>
                                    <li><a title="Google Map" href="<?php echo base_url() ?>kandidat/cetak"><i class="fa big-icon fa-files-o icon-wrap"></i><span class="mini-sub-pro">Data Kandidat</span></a></li>
                                    <li><a title="Google Map" href="<?php echo base_url() ?>vote/cetak"><i class="fa big-icon fa-files-o icon-wrap"></i><span class="mini-sub-pro">Data Vote</span></a></li>
                                    <!-- <li><a title="Data Maps" href="<?php echo base_url() ?>vote/cetak"><i class="fa big-icon fa-files-o icon-wrap"></i><span class="mini-sub-pro">Data Vote</span></a></li> -->
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if($this->session->userdata('logged_anggota')) : ?>
                            <li><a title="Landing Page" href="<?php echo base_url()?>vote" aria-expanded="false">
                                <i class="fa fa-hourglass sub-icon-mg" aria-hidden="true"></i><span class="mini-click-non">Vote</span></a>
                            </li>
                            <li><a title="Landing Page" href="<?php echo base_url() ?>vote/cetak" aria-expanded="false">
                                <i class="fa big-icon fa-desktop icon-wrap"></i> <span class="mini-click-non">Report</span></a>
                            </li>
                             <?php endif; ?>
                        <?php endif; ?>
                </nav>
            </div>
        </nav>
    </div>
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="index.html"><img class="main-logo" src="<?php echo base_url() ?>asset/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row">
                                    <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                        <div class="menu-switcher-pro">
                                            <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                                    <i class="fa fa-bars"></i>
                                                </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
                                        <div class="header-top-menu tabl-d-n">
                                            
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                        <div class="header-right-info">
                                            <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                                <?php if(!empty($notif)):?>
                                                <li class="nav-item dropdown">
                                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa fa-envelope-o adminpro-chat-pro" aria-hidden="true"></i><span class="indicator-ms"></span></a>
                                                    <div role="menu" class="author-message-top dropdown-menu animated zoomIn">
                                                        <div class="message-single-top">
                                                            <h1>Message</h1>
                                                        </div>
                                                        <ul class="message-menu">
                                                            <?php foreach($notif as $key) : ?>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="message-img">
                                                                        <img src="img/contact/1.jpg" alt="">
                                                                    </div>
                                                                    <div class="message-content">
                                                                        <span class="message-date"><?=date("Y-m-d")?></span>
                                                                        <h2><?php echo $this->session->userdata('ses_username');?></h2>
                                                                        <h2>Tentang : <?=$key->tentang_undangan;?></h2>
                                                                        <p>Tolong segera melakukan vote karena waktu vote telah tiba. <?=$key->tgl_undangan?></p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                             <?php endforeach; ?>
                                                        </ul>
                                                        <div class="message-view">
                                                            <a href="#">View All Messages</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                 <?php endif; ?>
                                              
                                                

                                                <li class="nav-item">
                                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                            <i class="fa fa-user adminpro-user-rounded header-riht-inf" aria-hidden="true"></i>
                                                            <span class="admin-name"><?=strtoupper( $this->session->userdata('ses_username')); ?></span>
                                                            <i class="fa fa-angle-down adminpro-icon adminpro-down-arrow"></i>
                                                        </a>
                                                        <?php $user = empty($this->session->userdata('ses_id_admin'))?$this->session->userdata('ses_id_anggota'):
                                                        $this->session->userdata('ses_id_admin'); ?>

                                                    <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                                                        <?php if (!empty($this->session->userdata('ses_id_anggota'))):?>
                                                        <li><a href="<?php echo base_url('anggota/detailanggota/'.$user)?>"><span class="fa fa-user author-log-ic"></span>My Profile</a>
                                                        </li>
                                                        <?php endif; ?>
                                                        <li>
                                                            <a href="<?php echo base_url();?>logout"><span class="fa fa-lock author-log-ic"></span>Log Out</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul class="mobile-menu-nav">
                                        <li><a data-toggle="collapse" data-target="#Charts" href="#">Dashboard <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#demo" href="#">Vote <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>                                        </li>
                                        <li><a data-toggle="collapse" data-target="#others" href="#">Data Kandidat <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                            <ul id="others" class="collapse dropdown-header-top">
                                                <li><a href="file-manager.html">SPV & Kordinator</a></li>
                                                <li><a href="contacts.html">Kasir & CSO</a></li>
                                                <li><a href="projects.html">SPG & Pram</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          