<div class="review-tab-pro-inner">
    <ul id="myTab3" class="tab-review-design">
    <li class="active"><a href="#description"><i class="fa fa-pencil" aria-hidden="true"></i>Edit Data Kandidat</a></li>
    </ul>
        <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
                <div class="row">
                    <?php foreach ($data_kandidat as $key) :?>
                    <?php echo form_open('kandidat/upeditkandidat/'.$key->id_kandidat); ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="review-content-section">
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <input type="text" disabled="" value="<?php echo  $key->nama_anggota;?>"  class="form-control" placeholder="Nama Kandidat">
                            </div>
                             <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <input type="text" value="<?php echo  $key->periode_kandidat;?>" name="periode_kandidat" class="form-control" required="" placeholder="Periode">
                            </div>

                            <select  name="level_kandidat" required="" class="form-control pro-edt-select form-control-primary">
                                <option value="" > PILIH JENIS KANDIDAT</option>
                                <option value="SPG PRAM">SPG PRAM</option>
                                <option value="CSO KASIR">CSO KASIR</option>
                                <option value="SPV COOR">SPV COOR</option>
                            </select>
                            <br>
                             <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-tag" aria-hidden="true"></i></span>
                                <textarea class="form-control" name="visi_kandidat" placeholder="visi"><?php echo  $key->visi_kandidat;?></textarea> 
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="review-content-section">
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <input type="text" value="<?php echo  $key->brand_kandidat;?>"  name="brand_kandidat" class="form-control" placeholder="Brand">
                            </div>
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <input type="text" value="<?php echo  $key->world_kandidat;?>" name="world_kandidat" class="form-control" placeholder=" World">
                            </div>
                              <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                <input type="text" value="<?php echo  $key->bidang_kandidat;?>"readonly="" name="bidang_kandidat" class="form-control" required="" placeholder="Bidang Kandidat">
                            </div>
                             <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-tag" aria-hidden="true"></i></span>
                                <textarea class="form-control" name="misi_kandidat"  placeholder="Misi"><?php echo  $key->misi_kandidat;?></textarea> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="text-center mg-b-pro-edt custom-pro-edt-ds">
                            <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Simpan
                                </button>
                            <a href="<?php echo base_url() ?>kandidat" type="button" class="btn btn-warning waves-effect waves-light">Batal
                                </a>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                     <?php endforeach; ?>
                </div>
            </div>
        </div>
</div>
