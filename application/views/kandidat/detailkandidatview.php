
            <!-- Mobile Menu end -->
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list single-page-breadcome">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="breadcome-heading">
                                            <form role="search" class="">
                                                <input type="text" placeholder="Search..." class="form-control">
                                                <a href=""><i class="fa fa-search"></i></a>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="#"></a> <span class="bread-slash"></span>
                                            </li>
                                            <li><span class="bread-blod">Detail Kandiadat</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Single pro tab start-->
        <?php foreach ($data_kandidat as $a) {
        
         ?>
        <div class="single-product-tab-area mg-t-15 mg-b-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <div id="myTabContent1" class="tab-content">
                            <div class="product-tab-list tab-pane fade active in" id="single-tab1">
                                <img src="<?php echo base_url() . 'asset/img/'.$a->foto_anggota?>" alt="" />
                            </div>
                        </div>
                       
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                        <div class="single-product-details res-pro-tb">
                            <h1><?php echo $this->session->userdata('ses_nama_admin'); ?></h1>
                            <div class="color-quality-pro">
                                <div class="clear"></div>
                                <div class="single-social-area">
                                    <h3>No Kandidat     :<?php echo $a->nik_anggota?></h3>
                                    <h3>Nama    : <?php echo $a->nama_anggota ?></h3>
                                    <h3>Bidang  : <?php echo $a->bidang_kandidat ?></h3>
                                    <h3>World   : <?php echo $a->world_kandidat ?></h3>
                                    <h3>Brand   : <?php echo $a->brand_kandidat ?></h3>
                                    <h3>Periode   : <?php echo $a->periode_kandidat ?></h3>
                                    <h3>Jabatan   : <?php echo $a->level_kandidat ?></h3>
                                </div>
                            </div>
                            <div class="single-pro-cn">
                                <h5>Visi</h5>
                                <p><?php echo $a->visi_kandidat ?></p>
                            </div>
                             <div class="single-pro-cn">
                                <h5>Misi</h5>
                                <p><?php echo $a->misi_kandidat ?></p>
                            </div>
                                <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   
       