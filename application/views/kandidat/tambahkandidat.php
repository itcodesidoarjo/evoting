<div class="review-tab-pro-inner">
    <ul id="myTab3" class="tab-review-design">
    <?php if($this->session->flashdata('pesan')): ?>
         <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                    </button>
                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                <p class="message-alert-none"><?=$this->session->flashdata('pesan')?></p>
            </div>
        <?php endif; ?>
         <?php if($this->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-danger-style1 alert-danger-stylenone">
                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                    </button>
                <i class="fa fa-check adminpro-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                <p class="message-alert-none"><?=$this->session->flashdata('error')?></p>
            </div>
        <?php endif; ?>
    <li class="active"><a href="#description"><i class="fa fa-pencil" aria-hidden="true"></i>Tambah Data Kandidat</a></li>
    </ul>
        <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
                <div class="row">
                    <?php echo form_open('kandidat/inputkandidat'); ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="review-content-section">
                            <div class="input-group custom-go-button">
                                <input type="text" id="nama_anggota" placeholder="Cari Anggota" readonly required class="form-control">
                                <span class="input-group-btn"><a class="btn btn-primary"href="#" data-toggle="modal" data-target="#cari"><i class="fa fa-search" aria-hidden="true"></i></a></span>
                            </div>
                            <br>
                            <input type="hidden" name="id_anggota" id="id_anggota">
                            <select name="level_kandidat" required="" class="form-control pro-edt-select form-control-primary">
                                <option value="" > PILIH JENIS KANDIDAT</option>
                                <option value="SPG PRAM">SPG PRAM</option>
                                <option value="CSO KASIR">CSO KASIR</option>
                                <option value="SPV COOR">SPV COOR</option>
                            </select>
                            <br>
                             <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                <input type="text" name="periode_kandidat" class="form-control" required="" placeholder="Periode">
                            </div>
                             <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-tag" aria-hidden="true"></i></span>
                                <textarea class="form-control" name="visi_kandidat" placeholder="visi"></textarea> 
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="review-content-section">
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                <input type="text" name="brand_kandidat" id="brand_kandidat" readonly class="form-control" placeholder="Brand">
                            </div>
                            <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                <input type="text" name="world_kandidat" id="world_kandidat" readonly class="form-control" placeholder=" World">
                            </div>
                              <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                <input type="text" name="bidang_kandidat" id="bidang_kandidat" readonly class="form-control" required="" placeholder="Bidang Kandidat">
                            </div>
                             <div class="input-group mg-b-pro-edt">
                                <span class="input-group-addon"><i class="fa fa-tag" aria-hidden="true"></i></span>
                                <textarea class="form-control" name="misi_kandidat"  placeholder="Misi"></textarea> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="text-center mg-b-pro-edt custom-pro-edt-ds">
                            <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Simpan
                                </button>
                            <a href="<?php echo base_url() ?>kandidat" type="button" class="btn btn-warning waves-effect waves-light">Batal
                                </a>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
</div>

<div class="modal fade" id="cari" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Cari Anggota</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="width:96%;margin:auto;">
                    <table class="table table-bordered table-hover" id="manageMemberTable">
                         <thead>
                            <tr>
                                <th data-field="id">No</th>
                                <th>No. Identitas</th>
                                <th>Nama</th>
                                <th>Bidang</th>
                                <th>Brand</th>
                                <th>Word</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no=1;
                            foreach($tbl_anggota as $key) : ?>
                            <tr onclick="pilih(<?php echo $key->id_anggota; ?>)" style="cursor:pointer;">
                                <td><?=$no++?></td>
                                <td><?=$key->nik_anggota?></td>
                                <td><?=$key->nama_anggota?></td>
                                <td><?=$key->bidang_anggota?></td>
                                <td><?=$key->brand_anggota?></td>
                                <td><?=$key->world_anggota?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script   src="https://code.jquery.com/jquery-3.3.1.js"   integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="   crossorigin="anonymous"></script>
        <script type="text/javascript">
            function getval(sel) {
                var tersembunyi;
                tersembunyi = document.getElementById("hidden");
                var tampil;
                tampil = document.getElementById("show");
                if (sel.value != 'Pokok Soal'){
                    tersembunyi.style.display = 'none';
                    tampil.style.display = 'block';
                }
                else {
                    tersembunyi.style.display = 'block';
                    tampil.style.display = 'none';
                }
            }
            function pilih(id){
                <?php 
                    $sql = $this->db->get('tbl_anggota');
                    foreach($sql->result() as $s){  
                ?>
                if (id == <?php echo $s->id_anggota; ?>) {
                    document.getElementById("nama_anggota").value="<?php echo $s->nama_anggota;?>";
                    document.getElementById("brand_kandidat").value="<?php echo $s->brand_anggota;?>";
                    document.getElementById("world_kandidat").value="<?php echo $s->world_anggota;?>";
                    document.getElementById("bidang_kandidat").value="<?php echo $s->bidang_anggota;?>";
                    document.getElementById("id_anggota").value="<?php echo $s->id_anggota;?>";
                    $("#cari").modal('hide');
                }
                <?php } ?>
               
            }
        </script>