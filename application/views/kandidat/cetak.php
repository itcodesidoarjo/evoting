<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<div class="page">
<div class="subpage">

    <table border="0" width="100%" style="text-align: left;">
        <thead>
            <tr>
                <th align="center" width="13%">
                    <img  style ="width:auto; height:auto; max-width:140px; max-height:200px; display:block;"  src="<?php echo base_url() ?>asset/img/logo/logo.png" alt="" /> </th>
                <th>  
                </th>
            </tr>
        </thead>
    </table>
     
     <hr style="margin-top:2px;">
     <h5 align="center" style="margin-top: 0px;">
        <b>Laporan Data Kandidat</b>
     </h5>
     <?php if (!empty($periode)): ?>
         <h5 align="center" style="margin-top:-18px">
             <b> <?= Html::encode(strtoupper($periode)) ?> </b>
         </h5>
     <?php endif; ?>
     <hr style="margin-top:0px; margin-bottom:5px; margin-top: -15px;">
<?php
    $no    = 1;
    
?>

    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="table-layout: fixed;" >
        <thead>
            <tr>
                <th align="center" style="padding: 2px 3px 2px 3px; height: 0.7cm;font-size:72%; border-bottom: 1px solid; width: 4%; ">
                    No
                </th>

                <th  style="padding: 2px 3px 2px 3px; height: 0.7cm; font-size:72%; border-bottom: 1px solid; width: 10%; ">
                    Tgl. Registrasi
                </th>
                <th style="padding: 2px 3px 2px 3px; height: 0.7cm;font-size:72%; border-bottom: 1px solid; width: 10%;  ">
                    Nama
                </th>
                <th style="padding: 2px 3px 2px 3px; height: 0.7cm; font-size:72%; border-bottom: 1px solid; width: 10%; ">
                    No Kandidat
                </th>
                <th  style="padding: 2px 3px 2px 3px; height: 0.7cm; font-size:72%; border-bottom: 1px solid; width: 10%; ">
                    Periode
                </th>

                <th  style="padding: 2px 3px 2px 3px; height: 0.7cm; font-size:72%; border-bottom: 1px solid; width: 10%; ">
                    Bidang
                </th>

                <th  style="padding: 2px 3px 2px 3px; height: 0.7cm; font-size:72%; border-bottom: 1px solid; width: 10%; ">
                    World
                </th>

                <th  style="padding: 2px 3px 2px 3px; height: 0.7cm; font-size:72%; border-bottom: 1px solid; width: 10%; ">
                    Brand
                </th>
            </tr>
        </thead>

            <?php 
                $no =1;                        
                foreach ($tbl_kandidat as $row1) :             
            ?>

            <tr>
                <td align="center" style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo $no ?>
                </td>

                <td style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo date('d M Y h:i:s', strtotime($row1->tglreg_kandidat)); ?>
                </td>
                <td style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo $row1->nama_anggota; ?>
                </td>
                <td style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo $row1->nik_anggota; ?>
                </td>

                <td align="center" style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo  $row1->periode_kandidat;?>
                </td>
                <td style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo $row1->bidang_kandidat; ?>
                </td>
                <td style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo $row1->world_kandidat; ?>
                </td>

                <td style="padding: 4px 4px 4px 4px; font-size:70%; ">
                    <?php echo $row1->brand_kandidat; ?>
                </td>


             <?php $no++; endforeach; ?>
          
                    </table>
                </td>
            </tr>

    </table>
               </div>
    </div>
</body>
</html>

<style type="text/css">
    
 #btm td{
    border-bottom: 1px solid black;
 }
    body{
    font-family : Arial;
    font-style: bold;
    margin: 0;
    background-color: #404040;
}
.page {
    width: 210mm;
    min-height:297mm ;
    padding: 1mm;
    margin: 0mm auto;
    background: white;
}
.subpage {
    margin-left:15mm;
    margin-right:5mm;
    margin-top:5mm;
}

table td {  
    word-wrap: break-word;         
    overflow-wrap: break-word;     
}

@page {
    size: A4;
    margin: 0;

}

@media print {
    .page {
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: always;
    }
}
</style>


